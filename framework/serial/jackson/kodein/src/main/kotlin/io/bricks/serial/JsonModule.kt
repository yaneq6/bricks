package io.bricks.serial

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import io.bricks.serial.core.StringJsonFactory
import io.bricks.serial.jackson.JacksonFactory

val jacksonFactoryModule = Kodein.Module("jacksonFactoryModule") {
    bind<StringJsonFactory>() with singleton { JacksonFactory() }
}

val jsonFactoryModule = jacksonFactoryModule
