package io.bricks.serial

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import io.bricks.serial.core.StringJsonFactory
import io.bricks.serial.gson.GsonFactory

val gsonFactoryModule = Kodein.Module("gsonFactoryModule") {
    bind<StringJsonFactory>() with singleton { GsonFactory() }
}

val jsonFactoryModule = gsonFactoryModule
