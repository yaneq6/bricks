package io.bricks.rx.core.comonent

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import io.bricks.rx.core.CompositeRxComponent
import io.bricks.rx.core.RxComponent
import io.bricks.rx.core.RxSchedulers
import io.reactivex.Scheduler

val rxComponentModule = Kodein.Module("rxComponentModule") {
    bind<RxComponent>() with provider { CompositeRxComponent(instance()) }
}

val dummyRxSchedulersModule = Kodein.Module("dummyRxSchedulersModule") {
    bind() from instance(RxSchedulers.dummy())
}

fun rxCustomSchedulersModule(main: Scheduler, background: Scheduler) = Kodein.Module("rxCustomSchedulersModule") {
    bind() from instance(RxSchedulers(main, background))
}