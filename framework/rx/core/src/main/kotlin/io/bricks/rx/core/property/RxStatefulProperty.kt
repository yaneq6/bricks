package io.bricks.rx.core.property

import io.reactivex.subjects.BehaviorSubject

open class RxStatefulProperty<T> private constructor(
    override val subject: BehaviorSubject<T>,
    onError: (Throwable) -> Unit
) :
    RxProperty<T>(subject, onError) {

    internal constructor(onError: (Throwable) -> Unit) : this(BehaviorSubject.create(), onError)

    fun get(): T = subject.value

    fun isEmpty() = subject.isEmpty

    override fun toString() =
        "${javaClass.simpleName}(value=${subject.run { if (hasValue()) value else null }})"
}