package io.bricks.rx.core.model

import io.bricks.rx.core.SchedulersWrapper
import io.bricks.rx.core.action.RxInteractor
import io.bricks.rx.core.property.Reference
import io.bricks.rx.core.property.RxPropertyDelegate
import io.bricks.rx.core.property.RxStatefulProperty
import io.bricks.rx.core.property.RxStatelessProperty
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

internal class BaseRxModel(
    private val rxInteractor: RxInteractor,
    private val schedulersWrapper: SchedulersWrapper,
    private val disposables: CompositeDisposable = CompositeDisposable(),
    private val onError: (RxModel, Throwable) -> Unit
) :
    RxModel,
    SchedulersWrapper by schedulersWrapper,
    RxInteractor by rxInteractor {

    private val onModelError = { error: Throwable -> onError(this, error)}

    override fun isDisposed() = false

    override fun dispose() {
        rxInteractor.dispose()
        disposables.dispose()
    }

    override fun <T> statelessDelegate(
        provideCache: ((String) -> Reference<T>)?
    ) = RxPropertyDelegate<RxModel, RxStatelessProperty<T>, T>(
        rxProperty = stateless(),
        provideCache = provideCache
    )

    override fun <T> statefulDelegate(
        provideCache: ((String) -> Reference<T>)?
    ) = RxPropertyDelegate<RxModel, RxStatefulProperty<T>, T>(
        rxProperty = stateful(),
        provideCache = provideCache
    )

    private fun <T> stateless() = RxStatelessProperty<T>(onModelError).add()

    private fun <T> stateful(default: T? = null) = RxStatefulProperty<T>(onModelError).add()
        .apply { default?.let(this::onNext) }

    private fun <T : Disposable> T.add() = also { disposables.add(it) }
}