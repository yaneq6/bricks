package io.bricks.rx.core.property

interface Reference<T> {
    var value: T
}