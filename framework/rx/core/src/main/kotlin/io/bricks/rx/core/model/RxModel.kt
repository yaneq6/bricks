package io.bricks.rx.core.model

import io.bricks.rx.core.SchedulersWrapper
import io.bricks.rx.core.action.RxInteractor
import io.bricks.rx.core.property.Reference
import io.bricks.rx.core.property.RxPropertyDelegate
import io.bricks.rx.core.property.RxStatefulProperty
import io.bricks.rx.core.property.RxStatelessProperty

interface RxModel : RxInteractor, SchedulersWrapper {

    fun <T> statefulDelegate(
        provideCache: ((String) -> Reference<T>)? = null
    ): RxPropertyDelegate<RxModel, RxStatefulProperty<T>, T>

    fun <T> statelessDelegate(
        provideCache: ((String) -> Reference<T>)? = null
    ): RxPropertyDelegate<RxModel, RxStatelessProperty<T>, T>
}