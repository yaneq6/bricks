package io.bricks.rx.core.action

sealed class ActionResult {
    data class Start(val action: Action) : ActionResult()
    data class Success(val action: Action) : ActionResult()
    data class Error(val action: Action, val throwable: Throwable) : ActionResult()
}