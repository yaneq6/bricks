package io.bricks.rx.core

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

interface SchedulersWrapper {
    val mainScheduler: Scheduler
    val backgroundScheduler: Scheduler
}

data class RxSchedulers(
    override val mainScheduler: Scheduler,
    override val backgroundScheduler: Scheduler
) : SchedulersWrapper {

    companion object {
        private val CURRENT_THREAD_SCHEDULER get() = Schedulers.from { it.run() }
        fun dummy() = RxSchedulers(CURRENT_THREAD_SCHEDULER, CURRENT_THREAD_SCHEDULER)
    }
}