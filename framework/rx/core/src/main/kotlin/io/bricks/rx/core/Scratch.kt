package io.bricks.rx.core

import io.bricks.rx.core.action.Action
import io.bricks.rx.core.model.RxModel
import io.reactivex.Observable

class Model(
    rxModel: RxModel
) : RxModel by rxModel {

    val property1 by statelessDelegate<Boolean>()

    val property2 by statefulDelegate<Int>()
}

class ViewModel(private val model: Model, rxModel: RxModel) : RxModel by rxModel {

    private val viewProperty1 by statelessDelegate<String>()
    private val viewProperty2 by statefulDelegate<Int>()
    private val viewProperty3 by statelessDelegate<String>()
    val source by statefulDelegate<String>()

    init {
        with(model) {
            viewProperty1.bind(property1) { map(Boolean::toString) }

            source.bind(viewProperty1, viewProperty3,
                operator = { o1, o2, _ -> Observable.merge(o1, o2) },
                function = sumString)
        }
    }

    private val sumString get() = { s1: String, s2: String -> s1 + s2 }

    override fun <A : Action> A.execute() {
        when (this) {
            is Action1 -> viewProperty1 += string
            else -> model.invoke(this)
        }
    }
}

data class Action1(val string: String) : Action
