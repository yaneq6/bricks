package io.bricks.rx.core.property

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class RxPropertyDelegate<in R, P : RxProperty<T>, T> internal constructor(
    internal val rxProperty: P,
    private val provideCache: ((String) -> Reference<T>)? = null
) : ReadOnlyProperty<R, P> {

    private var cache: Reference<T>? = null

    val name: String get() = rxProperty.name

    override fun getValue(thisRef: R, property: KProperty<*>): P {
        if (rxProperty.propertyName == null) {
            property.name.let { name ->
                rxProperty.propertyName = name
                cache = provideCache
                    ?.invoke(name)
                    ?.connect()
            }
        }
        return rxProperty
    }

    private fun Reference<T>.connect() = also { rxProperty.connect(it) }
}