package io.bricks.rx.core.property

import io.reactivex.subjects.PublishSubject

class RxStatelessProperty<T> internal constructor(
    handleError: (Throwable) -> Unit
) : RxProperty<T>(
    subject = PublishSubject.create(),
    handleError = handleError
)