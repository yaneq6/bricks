package io.bricks.rx.core.action

import io.bricks.rx.core.RxSchedulers
import io.bricks.rx.core.SchedulersWrapper
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.toSingle
import io.reactivex.subjects.PublishSubject

class BaseRxInteraction internal constructor(
    rxSchedulers: RxSchedulers
) : RxInteractor, SchedulersWrapper by rxSchedulers {

    private val disposables = mutableMapOf<Action, Disposable>()

    override val results = PublishSubject.create<ActionResult>()!!

    override fun <A : Action> A.execute() = throw NotImplementedError()

    override fun <A: Action>A.run(execute: A.() -> Unit) {
        start()
        disposables[this] = toSingle()
            .observeOn(backgroundScheduler)
            .map { execute() }
            .subscribeOn(mainScheduler)
            .doOnEvent { _, _ -> dispose() }
            .subscribe(
                { success() },
                { error(it) })
    }

    private fun Action.start() = ActionResult.Start(this).notify()
    private fun Action.success() = ActionResult.Success(this)
    private fun Action.error(throwable: Throwable) = ActionResult.Error(this, throwable)
    private fun Action.dispose() = disposables.remove(this)

    override fun isDisposed(): Boolean = false

    private fun ActionResult.notify() = results.onNext(this)

    override fun dispose() {
        disposables.apply {
            values.forEach { it.dispose() }
            clear()
        }
    }
}