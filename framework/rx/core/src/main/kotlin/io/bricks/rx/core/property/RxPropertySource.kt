package io.bricks.rx.core.property

import io.reactivex.ObservableSource
import io.reactivex.Observer

interface RxPropertySource<T> : ObservableSource<T>

interface RxPropertyObserver<T> : Observer<T>