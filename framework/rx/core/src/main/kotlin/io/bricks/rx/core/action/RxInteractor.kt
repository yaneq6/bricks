package io.bricks.rx.core.action

import io.reactivex.Observable
import io.reactivex.disposables.Disposable

interface RxInteractor : Disposable {

    val results: Observable<ActionResult>

    fun <A : Action>invoke(action: A) {
        action.run { execute() }
    }
    fun <A : Action>A.run(execute: A.() -> Unit)
    fun <A : Action>A.execute()
}