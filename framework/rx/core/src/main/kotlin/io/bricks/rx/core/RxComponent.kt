package io.bricks.rx.core

import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign

interface RxComponent {

    val disposables: CompositeDisposable
    val schedulers: RxSchedulers

    fun <T> Observable<T>.blocking(block: T.() -> Unit) {
        +observeOn(schedulers.mainScheduler).subscribe(block)
    }

    fun <T> Observable<T>.async(block: (T) -> Unit) {
        +observeOn(schedulers.backgroundScheduler).subscribe(block)
    }

    fun Completable.async(block: () -> Unit) {
        +observeOn(schedulers.backgroundScheduler).subscribe(block)
    }

    fun <T> Maybe<T>.async(block: (T) -> Unit) {
        +observeOn(schedulers.backgroundScheduler).subscribe(block)
    }

    operator fun Disposable.unaryPlus() {
        disposables += this
    }

    fun unbind() = disposables.clear()
}

interface RxComponentDelegate : RxComponent {
    val rxComponent: RxComponent

    override val disposables get() = rxComponent.disposables
    override val schedulers get() = rxComponent.schedulers
}

open class CompositeRxComponent(
    override val schedulers: RxSchedulers
) : RxComponent {
    override val disposables = CompositeDisposable()
}