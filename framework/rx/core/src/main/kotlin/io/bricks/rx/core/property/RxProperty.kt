package io.bricks.rx.core.property

import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.Subject

open class RxProperty<T> internal constructor(
    internal open val subject: Subject<T>,
    private val handleError: (Throwable) -> Unit,
    internal var propertyName: String? = null,
    private val disposables: CompositeDisposable = CompositeDisposable()
) :
    RxPropertyObserver<T>,
    RxPropertySource<T>,
    ObservableSource<T> by subject,
    Observer<T> by subject,
    SingleObserver<T>,
    Disposable {

    var name: String
        get() = propertyName ?: "uninitialized ${this::class.java.simpleName}"
        set(value) {
            propertyName = value
        }

    override fun isDisposed() = disposables.isDisposed

    override fun dispose() = disposables.clear()

    override fun onSubscribe(disposable: Disposable) {
        subject.onSubscribe(disposable)
        if (!disposable.isDisposed) disposables.add(disposable)
    }

    operator fun plusAssign(value: T) = onNext(value)

    override fun onSuccess(t: T) = onNext(t)

    override fun onError(e: Throwable) = handleError.invoke(e)

    private fun <T : Disposable> T.add() = apply { disposables.add(this) }

    fun connect(reference: Reference<T>) = apply {
        onNext(reference.value)
        toObservable().subscribe(reference::value::set, this::onError).add()
    }

    fun <T> ObservableSource<T>.toObservable() = (this as? Observable ?: Observable.wrap(this))!!

    fun <T> RxPropertyObserver<T>.bind(
        source: RxPropertySource<T>,
        on: Scheduler? = null
    ) = apply {
        source.toObservable()
            .optionalSubscribeOn(on)
            .subscribe(this)
    }

    fun <T1> bind(
        source: RxPropertySource<T1>,
        on: Scheduler? = null,
        transformer: Observable<T1>.() -> ObservableSource<T>
    ) = apply {
        transformer.invoke(
            source.toObservable().optionalSubscribeOn(on)
        ).subscribe(this)
    }

    fun <T1, T2> bind(
        source1: RxPropertySource<T1>,
        source2: RxPropertySource<T2>,
        on: Scheduler? = null,
        operator: (
            Observable<T1>,
            Observable<T2>,
            (T1, T2) -> T
        ) -> ObservableSource<T>,
        function: (T1, T2) -> T
    ) = apply {
        operator(
            source1.toObservable().optionalSubscribeOn(on),
            source2.toObservable().optionalSubscribeOn(on),
            function
        ).subscribe(this)
    }

    fun <T1, T2, T3> bind(
        source1: RxPropertySource<T1>,
        source2: RxPropertySource<T2>,
        source3: RxPropertySource<T3>,
        on: Scheduler? = null,
        operator: (
            Observable<T1>,
            Observable<T2>,
            Observable<T3>,
            (T1, T2, T3) -> T
        ) -> ObservableSource<T>,
        transformer: (T1, T2, T3) -> T
    ) = apply {
        operator(
            source1.toObservable().optionalSubscribeOn(on),
            source2.toObservable().optionalSubscribeOn(on),
            source3.toObservable().optionalSubscribeOn(on),
            transformer
        ).subscribe(this)
    }

    fun <T1, T2, T3, T4> bind(
        source1: RxPropertySource<T1>,
        source2: RxPropertySource<T2>,
        source3: RxPropertySource<T3>,
        source4: RxPropertySource<T4>,
        on: Scheduler? = null,
        operator: (
            Observable<T1>,
            Observable<T2>,
            Observable<T3>,
            Observable<T4>,
            (T1,T2,T3,T4)->T
        ) -> ObservableSource<T>,
        transformer: (T1,T2,T3,T4)->T
    ) = apply {
        operator(
            source1.toObservable().optionalSubscribeOn(on),
            source2.toObservable().optionalSubscribeOn(on),
            source3.toObservable().optionalSubscribeOn(on),
            source4.toObservable().optionalSubscribeOn(on),
            transformer
        ).subscribe(this)
    }

    private fun <T> Observable<T>.optionalSubscribeOn(scheduler: Scheduler?) =
        (if (scheduler == null) this
        else subscribeOn(scheduler))!!
}