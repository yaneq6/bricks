package io.bricks.rx.core

import io.reactivex.subjects.BehaviorSubject

fun <T> BehaviorSubject<List<T>>.blocking(reduce: List<T>.() -> List<T>) = onNext(reduce(value))

operator fun <T> BehaviorSubject<List<T>>.plus(list: List<T>) = onNext(list)

operator fun <T> BehaviorSubject<List<T>>.plusAssign(t: T) = onNext(
    if (hasValue()) value + t
    else listOf(t)
)