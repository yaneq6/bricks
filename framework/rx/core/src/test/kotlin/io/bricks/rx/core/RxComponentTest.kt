package io.bricks.rx.core

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers.from
import io.reactivex.subjects.PublishSubject
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.concurrent.Executor

class RxComponentTest : RxComponent {
    private val mainSubject = PublishSubject.create<Runnable>()

    private val backgroundSubject = PublishSubject.create<Runnable>()
    private val mainExecutor = Executor { it.also(mainSubject::onNext).run() }
    private val backgroundExecutor = Executor { it.also(backgroundSubject::onNext).run() }

    override val schedulers = RxSchedulers(from(mainExecutor), from(backgroundExecutor))
    override var disposables = CompositeDisposable()

    @After
    fun tearDown() {
        disposables.dispose()
        disposables = CompositeDisposable()
    }

    @Test
    fun shouldAddDisposable() {
        assertEquals(0, disposables.size())
        +CompositeDisposable()
        assertEquals(1, disposables.size())
    }

    @Test
    fun shouldSubscribeObservableOnMainThread() {
        var calls = 0
        var called = false
        mainSubject.subscribe { ++calls }
        Observable.just(Unit).blocking { called = true }

        assertTrue(called)
        assertEquals(1, calls)
    }

    @Test
    fun shouldSubscribeObservableOnBackgroundThread() {
        var calls = 0
        var called = false
        backgroundSubject.subscribe { ++calls }
        Observable.just(Unit).async { called = true }

        assertTrue(called)
        assertEquals(1, calls)
    }

    @Test
    fun shouldSubscribeCompletableOnBackgroundThread() {
        var calls = 0
        var called = false
        backgroundSubject.subscribe { ++calls }
        Completable.complete().async { called = true }

        assertTrue(called)
        assertEquals(1, calls)
    }

    @Test
    fun shouldClearAndDisposeAllSubscribers() {
        Observable.fromPublisher<Unit> {  }.blocking {  }
        unbind()
        assertEquals(0, disposables.size())
    }

}