package io.bricks.rx.core.property

import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class RxPropertyTest {

    @RelaxedMockK lateinit var disposable: Disposable
    @RelaxedMockK lateinit var handleError: (Throwable) -> Unit
    @RelaxedMockK lateinit var reference: Reference<Any>
    private lateinit var compositeDisposable: CompositeDisposable
    private lateinit var subject: Subject<Any>
    private lateinit var property: RxProperty<Any>
    private lateinit var output: TestObserver<Any>

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        compositeDisposable = CompositeDisposable()
        subject = PublishSubject.create()
        property = RxProperty(
            disposables = compositeDisposable,
            subject = subject,
            handleError = handleError
        )
        output = subject.test()
    }

    @Test
    fun isDisposed() {
        assertEquals(compositeDisposable.isDisposed, property.isDisposed)
    }

    @Test
    fun dispose() {
        property.dispose()
        assertFalse(compositeDisposable.isDisposed)
    }

    @Test
    fun onSubscribe() {
        every { disposable.isDisposed } returns false

        property.onSubscribe(disposable)
        verify(exactly = 1) { disposable.isDisposed }
    }

    @Test
    fun onNext() {
        property.onNext(Unit)
        output.assertValue(Unit)
    }

    @Test
    fun onError() {
        val error = Exception()
        property.onError(error)
        verify(exactly = 1) { handleError.invoke(error) }
    }

    @Test
    fun onComplete() {
        property.onComplete()
        output.assertComplete()
    }

    @Test
    fun connect() {
        every { reference.value } returns Unit

        property.connect(reference)

        output.assertValue(Unit)

        property += 0

        output.assertValues(Unit, 0)

        verify(exactly = 1) { reference.value = 0 }
    }

    @Test
    fun subscribe() {
    }
}