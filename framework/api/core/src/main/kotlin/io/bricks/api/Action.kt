package io.bricks.api

interface Action<out R>

typealias ActionClass = Class<out Action<*>>