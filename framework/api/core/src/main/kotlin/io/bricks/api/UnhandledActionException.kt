package io.bricks.api

class UnhandledActionException : Exception {

    constructor(type: Class<*>) : super("Unhandled action type: ${type.name}")
    constructor(action: Action<*>) : super("Unhandled action: $action, of class: ${action.javaClass.name}")

}