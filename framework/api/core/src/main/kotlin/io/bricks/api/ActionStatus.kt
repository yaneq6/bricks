package io.bricks.api

sealed class ActionStatus {
    abstract val action: Action<*>

    data class Start(override val action: Action<*>): ActionStatus()

    data class Success(override val action: Action<*>, val data: Any = Unit): ActionStatus()

    data class Stop(override val action: Action<*>): ActionStatus()

    data class Error(override val action: Action<*>, val throwable: Throwable): ActionStatus() {

        override fun hashCode(): Int =
            action.hashCode() * 31 +
                throwable.javaClass.hashCode() +
                throwable.localizedMessage.hashCode()

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Error) return false
            if (action != other.action) return false
            if (throwable::class != other.throwable::class) return false
            if (throwable.localizedMessage != other.throwable.localizedMessage) return false
            return true
        }
    }
}