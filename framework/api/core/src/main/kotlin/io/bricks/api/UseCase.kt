package io.bricks.api

import kotlin.reflect.KClass

interface UseCase<in A, out R> {
    fun A.execute(): R
}

operator fun <A: Action<R>, R> UseCase<A, R>.invoke(action: A): R = run { action.execute() }

typealias TypedInteractor = Pair<KClass<*>, UseCase<*, *>>
typealias Interactors = Set<TypedInteractor>