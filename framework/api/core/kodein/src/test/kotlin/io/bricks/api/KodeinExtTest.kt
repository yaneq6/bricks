package io.bricks.api

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.kodein.di.Kodein
import org.kodein.di.generic.*

private typealias ExampleEntry = Pair<String, String>
private typealias ExampleEntries = Set<ExampleEntry>

class InteractorTest {

    object Action1 : Action<Unit>
    object Action2 : Action<Unit>
    class Interactor1 : UseCase<Action1, Unit> { override fun Action1.execute() = Unit }
    class Interactor2 : UseCase<Action2, Unit> { override fun Action2.execute() = Unit }

    @Test
    fun testInteractorsBinding() {
        Kodein {
            bindInteractors()
            bindInteractor { Interactor1() }
            bindInteractor { Interactor2() }
        }.run {
            val interactors by interactors()
            assertNotNull(interactors)
            assertEquals(interactors.size, 2)
        }
    }

    @Test
    fun testKodeinSetBindings() {
        Kodein {
            bind() from setBinding<ExampleEntry>()
            bind<ExampleEntry>().inSet() with provider { "foo" to "foo" }
            bind<ExampleEntry>().inSet() with provider { "bar" to "bar" }
        }.run {
            val exampleSet by instance<ExampleEntries>()
            assertNotNull(exampleSet)
            assertEquals(exampleSet.size, 2)
        }
    }
}