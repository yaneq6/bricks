package io.bricks.api

import org.kodein.di.Kodein
import org.kodein.di.bindings.NoArgBindingKodein
import org.kodein.di.generic.*

inline fun <reified A: Action<R>, R> Kodein.Builder.bindInteractor(
    crossinline creator: NoArgBindingKodein<Any?>.() -> UseCase<A, R>
) {
    bind<TypedInteractor>().inSet() with provider {
        A::class to creator()
    }
}

fun Kodein.Builder.bindInteractors() = bind() from setBinding<TypedInteractor>()
fun Kodein.interactors() = instance<Interactors>()