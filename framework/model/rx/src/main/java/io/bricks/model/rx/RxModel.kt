package io.bricks.model.rx

import io.bricks.interaction.rx.handler.RxObservableActionHandler
import io.bricks.rx.core.RxComponent
import io.reactivex.subjects.BehaviorSubject

interface RxModel:
    RxObservableActionHandler,
    RxComponent

class RxModelImpl(
    handler: RxObservableActionHandler,
    rxComponent: RxComponent
): RxModel,
    RxObservableActionHandler by handler,
    RxComponent by rxComponent
