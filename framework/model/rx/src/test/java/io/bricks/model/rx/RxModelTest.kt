package io.bricks.model.rx

import io.bricks.api.Action
import io.bricks.interaction.rx.handler.RxObservableActionHandlerImpl
import io.bricks.interaction.rx.interactor.RxUseCase
import io.bricks.rx.core.CompositeRxComponent
import io.bricks.rx.core.RxSchedulers
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import org.junit.Test

class RxModelTest {

    @Test
    fun test() {
        // given
        val testState =  TestState()
        val rxSchedulers = RxSchedulers.dummy()
        val model = TestModel(
            state = testState,
            interactor = SetValueInteractor(testState),
            rxModel = RxModelImpl(
                handler = RxObservableActionHandlerImpl(rxSchedulers),
                rxComponent = CompositeRxComponent(rxSchedulers)
            )
        )
        val observable = model.value.test()

        // then
        model.setValue.onNext("test")

        // when
        observable.assertValue { it == "test" }
    }
}



private class TestModel(
    state: TestState,
    rxModel: RxModel,
    interactor: RxUseCase<SetValue, Unit>
) : RxModel by rxModel {

    val value = state.value

    val setValue: Observer<String> = PublishSubject.create<String>()
        .apply { map(::SetValue).handleWith(interactor).subscribe() }

}

private class TestState {

    val value = BehaviorSubject.create<String>()!!
}

data class SetValue(val value: String) : Action<Unit>


private class SetValueInteractor(
    val state: TestState
) : RxUseCase<SetValue, Unit> {

    override fun Observable<SetValue>.execute() = this
        .map(SetValue::value)
        .map(state.value::onNext)!!
}
