package io.bricks.model.rx.databinding

import android.databinding.ObservableField
import io.bricks.model.rx.RxModel
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

interface DataBindingRxModel : RxModel {
    fun <T> field() = ObservableField<T>()

    fun <T> ObservableField<T>.setter(observable: () -> Observable<T>) = apply {
        +observable().observeOn(schedulers.mainScheduler).subscribe { set(it) }
    }

    fun <T> ObservableField<T>.getter(observable: Observable<T>.() -> Observable<out Any>) = apply {
        val subject = PublishSubject.create<T>().apply {
            +subscribeOn(schedulers.backgroundScheduler).observable().subscribe({}, {})
        }

        addOnPropertyChangedCallback(object: android.databinding.Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(
                sender: android.databinding.Observable?,
                propertyId: Int
            ) {
                get()?.let(subject::onNext)
            }
        })
    }

    fun <T> Subject<T>.field(): ObservableField<T> = ObservableField<T>().apply {
        onPropertyChangedCallback { _, _ ->
            get()?.let { onNext(it) }
        }.let {
            addOnPropertyChangedCallback(it)

            +subscribeOn(
                schedulers.backgroundScheduler
            ).doOnDispose {
                removeOnPropertyChangedCallback(it)
            }.subscribe {
                set(it)
            }
        }
    }
}

private fun onPropertyChangedCallback(
    callback: (sender: android.databinding.Observable, propertyId: Int) -> Unit
) = object: android.databinding.Observable.OnPropertyChangedCallback() {

    override fun onPropertyChanged(
        sender: android.databinding.Observable,
        propertyId: Int
    ) = callback(sender, propertyId)
}

fun <T> Observer<T>.field() = ObservableField<T>().apply {
    addOnPropertyChangedCallback(onPropertyChangedCallback { _, _ -> get()?.let { onNext(it) } })
}

fun <T> Observable<T>.field() = ObservableField<T>().apply { subscribe { set(it) } }

fun <T> ObservableField<T>.observable() = Observable.fromPublisher<T> {
    addOnPropertyChangedCallback(onPropertyChangedCallback { _, _ ->
        it.apply { get()?.let { onNext(it) } }
    })
}!!

fun <T> ObservableField<T>.observer() = object: Observer<T> {

    var field: ObservableField<T>? = this@observer

    override fun onSubscribe(d: Disposable) {}

    override fun onComplete() {
        field = null
    }

    override fun onNext(t: T) {
        field?.set(t)
    }

    override fun onError(e: Throwable) {
        field = null
        e.printStackTrace()
    }
}