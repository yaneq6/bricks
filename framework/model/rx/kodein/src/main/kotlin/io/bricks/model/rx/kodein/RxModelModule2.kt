package io.bricks.model.rx.kodein

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import io.bricks.model.rx.RxModel
import io.bricks.model.rx.RxModelImpl

val rxModelModule = Kodein.Module("rxModelModule") {
    bind<RxModel>() with provider { RxModelImpl(instance(), instance()) }
}