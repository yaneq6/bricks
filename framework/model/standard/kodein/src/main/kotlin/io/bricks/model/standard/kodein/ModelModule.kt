package io.bricks.model.standard.kodein

import io.bricks.model.standard.Model
import io.bricks.model.standard.ModelImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val modelModule = Kodein.Module("modelModule") {
    bind<Model>() with singleton { ModelImpl(instance(), instance()) }
}