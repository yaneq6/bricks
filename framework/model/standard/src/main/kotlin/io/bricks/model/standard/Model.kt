package io.bricks.model.standard

import io.bricks.interaction.standard.handler.ActionHandler
import io.bricks.rx.core.RxComponent

interface Model:
    ActionHandler,
    RxComponent

class ModelImpl(
    handler: ActionHandler,
    rxComponent: RxComponent
): Model,
    ActionHandler by handler,
    RxComponent by rxComponent