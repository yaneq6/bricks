package io.bricks.network

import io.bricks.network.core.ApiAdapter
import io.bricks.network.fuel.FuelApiAdapter
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val fuelApiAdapterModule = Kodein.Module("fuelApiAdapterModule") {
    bind<ApiAdapter>() with singleton {
        FuelApiAdapter(
            instance(),
            instance()
        )
    }
}

val apiAdapterModule = fuelApiAdapterModule