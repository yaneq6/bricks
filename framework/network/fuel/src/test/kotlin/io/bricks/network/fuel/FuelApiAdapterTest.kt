package io.bricks.network.fuel

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.Client
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Response
import io.bricks.network.core.ApiAdapter
import io.bricks.network.core.ApiMethod
import io.bricks.serial.core.JsonSource
import io.bricks.serial.core.StringJsonFactory
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class FuelApiAdapterTest {


    private val config = ApiAdapter.Config(host = "test.host")

    private val log: (Any) -> Unit = {}

    @MockK
    private lateinit var client: Client

    @MockK
    private lateinit var method: ApiMethod<Any>

    @MockK
    private lateinit var jsonFactory: StringJsonFactory

    private lateinit var apiAdapter: FuelApiAdapter

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        mockkStatic(Fuel::class, Fuel.Companion::class)

        every {
            jsonFactory.run { any<JsonSource<String>>().deserialize<Unit>() }
        } returns Unit

        every {
            jsonFactory.run { any<Any>().serialize() }
        } returns JsonSource(
            dataType = String::class,
            source = ""
        )

        every {
            client.executeRequest(any())
        } returns Response(
            url = mockk(),
            statusCode = 200
        )

        every {
            method.resultType
        } returns Any::class.java

        FuelManager.instance.client = client

        apiAdapter = FuelApiAdapter(
            config = config,
            jsonFactory = jsonFactory,
            log = log
        )
    }

    @Test
    fun call() {
        val expected = Unit
        val actual = apiAdapter.call(method)

        assertEquals(expected, actual)
    }
}