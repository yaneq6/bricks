package io.bricks.network.fuel

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.Request
import io.bricks.network.core.ApiAdapter
import io.bricks.network.core.ApiMethod
import io.bricks.serial.core.JsonSource
import io.bricks.serial.core.StringJsonFactory

class FuelApiAdapter(

    override val config: ApiAdapter.Config,
    jsonFactory: StringJsonFactory,
    private val log: (Any) -> Unit = ::println

) : ApiAdapter,
    StringJsonFactory by jsonFactory {

    override fun <M: ApiMethod<T>, T: Any> call(method: M): T {
        val (body, error) = method
            .request().also { log("===REQUEST===") }.also { log(it.cUrlString()) }.also(log)
            .response().also { log("===RESPONSE===") }.also(log)
            .third.also { println() }

        if (error != null) throw error

        return JsonSource(method.resultType.kotlin, String(body!!)).deserialize()
    }

    private fun <T : Any> ApiMethod<T>.request() = Fuel.post(uri).json(serialize())

    private fun Request.json(json: JsonSource<String>): Request = body(json.source)
}