package io.bricks.network.rx

import io.bricks.api.ActionStatus
import io.bricks.network.core.ApiAdapter
import io.bricks.network.core.ApiMethod
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxApiAdapterWrapper(
    private val apiAdapter: ApiAdapter
): RxApiAdapter {

    override val config get() = apiAdapter.config

    override val status = PublishSubject.create<ActionStatus>()!!

    override fun <M: ApiMethod<T>, T: Any> invoke(method: M): Observable<T> = Observable.fromPublisher {
        try {
            status.onNext(ActionStatus.Start(method))
            val result = apiAdapter.call(method)

            it.onNext(result)
            status.onNext(ActionStatus.Success(method, result))
            it.onComplete()
            status.onNext(ActionStatus.Stop(method))
        } catch (e: Throwable) {
            it.onError(e)
            status.onNext(ActionStatus.Error(method, e))
        }
    }
}