package io.bricks.network.rx

import io.bricks.api.ActionStatus
import io.bricks.network.core.ApiAdapter
import io.bricks.network.core.ApiMethod
import io.reactivex.Observable

interface RxApiAdapter {

    val config: ApiAdapter.Config

    val status: Observable<ActionStatus>

    operator fun <M: ApiMethod<T>, T: Any> invoke(method: M): Observable<T>

}

