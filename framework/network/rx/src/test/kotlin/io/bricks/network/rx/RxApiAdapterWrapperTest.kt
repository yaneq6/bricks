package io.bricks.network.rx

import io.bricks.network.core.ApiAdapter
import io.bricks.network.core.ApiMethod
import io.mockk.every
import io.mockk.mockk
import org.junit.Test

class RxApiAdapterWrapperTest {

    private val apiAdapter = mockk<ApiAdapter>()
    private val rxApiAdapter = RxApiAdapterWrapper(apiAdapter)

    @Test
    operator fun invoke() {
        val response = "response"
        every { apiAdapter.call(TestApiMethod) } returns response

        val test = rxApiAdapter(TestApiMethod).test()

        test.assertValue(response)
    }

    object TestApiMethod: ApiMethod<String> {
        override val resultType = String::class.java
    }
}