package io.bricks.network.rx

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val rxApiAdapterModule = Kodein.Module("rxApiAdapterModule") {
    bind<RxApiAdapter>() with singleton { RxApiAdapterWrapper(instance()) }
}