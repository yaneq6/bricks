package io.bricks.network.core

import org.junit.Assert.assertEquals
import org.junit.Test

class ApiMethodTest {

    @Test
    fun getResultType() = assertEquals(
        Int::class.java,
        method.resultType
    )

    @Test
    fun getMethodName() = assertEquals(
        "io.bricks.network.core.ApiMethodTest\$TestMethod",
        method.methodName
    )

    companion object {
        private const val ARG = 0
        private val method = TestMethod(ARG)
    }

    private data class TestMethod(
        val arg: Int
    ): ApiMethod<Int> {
        override val resultType = Int::class.java
    }
}