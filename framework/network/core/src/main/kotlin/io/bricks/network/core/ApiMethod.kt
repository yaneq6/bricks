package io.bricks.network.core

import io.bricks.api.Action

interface ApiMethod<T>: Action<T> {
    val resultType: Class<T>
}

val ApiMethod<*>.methodName get() = javaClass.name!!
