package io.bricks.network.core

interface ApiAdapter {
    val config: Config

    fun <M: ApiMethod<T>, T: Any> call(method: M): T

    data class Config(
        val protocol: String = "http",
        val host: String = ""
    )

    val <T : Any>ApiMethod<T>.uri get() = with(config) { "$protocol://$host/$methodName" }
}