package io.bricks.android.ui

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

open class RxAdapter<T, VH: RecyclerView.ViewHolder>(
    observableList: Observable<List<T>>,
    @LayoutRes layoutResId: Int,
    createViewHolder: (View) -> VH,
    bindData: VH.(T) -> Unit
): RecyclerViewAdapter<T, VH>(
    layoutResId = layoutResId,
    createViewHolder = createViewHolder,
    bindData = bindData
) {

    private val disposable: Disposable = observableList
        .doAfterNext { notifyDataSetChanged() }
        .subscribe { items = it }

    fun unbind() = disposable.dispose()
}

open class RecyclerViewAdapter<T, VH: RecyclerView.ViewHolder>(
    @LayoutRes private val layoutResId: Int,
    private val createViewHolder: (View) -> VH,
    private val bindData: VH.(T) -> Unit
): RecyclerView.Adapter<VH>() {

    var items: List<T> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private fun ViewGroup.inflate(
        @LayoutRes layoutResId: Int,
        attach: Boolean = false
    ) = LayoutInflater.from(context).inflate(layoutResId, this, attach)!!

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = createViewHolder(parent.inflate(layoutResId))

    override fun onBindViewHolder(
        holder: VH,
        position: Int
    ) = holder.bindData(items[position])
}

class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)