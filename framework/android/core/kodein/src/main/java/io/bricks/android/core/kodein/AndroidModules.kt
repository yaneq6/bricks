package io.bricks.android.core.kodein

import io.bricks.rx.core.RxSchedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

val schedulersModule = Kodein.Module("schedulersModule") {
    bind() from singleton { RxSchedulers(AndroidSchedulers.mainThread(), Schedulers.io()) }
}