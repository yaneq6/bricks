package io.bricks.interaction.standard.handler

import io.bricks.api.Action
import io.bricks.api.ActionStatus
import io.bricks.api.ActionStatus.*
import io.bricks.api.UseCase
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

interface ObservableActionStatus {
    val status: Observable<ActionStatus>
}

interface SubjectActionStatus: ObservableActionStatus {
    override val status: PublishSubject<ActionStatus>
}

inline fun <reified AS: ActionStatus, reified A: Action<*>> Observable<ActionStatus>.filter() = this
    .filter { it is AS && it.action is A }
    .map { it.action as A }!!

interface ActionHandler: SubjectActionStatus {

    infix operator fun <A: Action<R>, R> UseCase<A, R>.invoke(action: A)
}

class ActionHandlerImpl: ActionHandler {


    override val status = PublishSubject.create<ActionStatus>()!!

    override fun <A: Action<R>, R> UseCase<A, R>.invoke(action: A) = action.executeBy(this)

    private fun <A: Action<R>, R> A.executeBy(
        useCase: UseCase<A, R>
    ) {
        try {
            start()
            useCase.run { execute() }
            success()
        } catch (e: Throwable) {
            error(e)
        } finally {
            stop()
        }
    }

    private fun Action<*>.start() = status.onNext(Start(this))
    private fun Action<*>.stop() = status.onNext(Stop(this))
    private fun Action<*>.success() = status.onNext(Success(this))
    private fun Action<*>.error(throwable: Throwable) = status.onNext(Error(this, throwable))
}