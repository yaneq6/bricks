package io.bricks.interaction.standard.handler

import io.bricks.api.*
import io.bricks.api.ActionStatus.*
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

interface RecursiveActionHandler {
    val status: Observable<ActionStatus>
    operator fun Action<*>.invoke(): Any
}

class RecursiveActionHandlerImpl(
    private val interactors: Map<Class<out Action<*>>, RawInteractor>
): RecursiveActionHandler {

    constructor(
        vararg interactors: Pair<Class<out Action<*>>, RawInteractor>
    ) : this(interactors.toMap())

    override val status = PublishSubject.create<ActionStatus>()!!

    override operator fun Action<*>.invoke(): Any = interactors[javaClass]
        .throwIfNotExist(this)
        .tryExecute(this)
        .tryExecuteResult()

    private fun RawInteractor?.throwIfNotExist(action: Action<*>) = this
        ?: throw UnhandledActionException(action)

    @Suppress("UNCHECKED_CAST")
    private fun RawInteractor.tryExecute(action: Action<*>): Any = with(action) {
        var result: Any
        start()
        try {
            result = invoke(action)
            success()
        } catch (e: Throwable) {
            result = e
            error(e)
        }
        stop()
        return result
    }

    private fun Action<*>.start() = status.onNext(Start(this))
    private fun Action<*>.stop() = status.onNext(Stop(this))
    private fun Action<*>.success() = status.onNext(Success(this))
    private fun Action<*>.error(throwable: Throwable) = status.onNext(Error(this, throwable))

    private fun Any.tryExecuteResult() = (this as? Action<*>)?.invoke() ?: this
}

private typealias RawInteractor = (Action<*>) -> Any

@Suppress("UNCHECKED_CAST")
fun UseCase<*, Any>.toRawInteractor() =
    (this as UseCase<Action<Any>, Any>)::invoke as (Action<*>) -> Any

fun rawInteractors(
    vararg useCases: Pair<Class<out Action<*>>, UseCase<*, Any>>
): Map<Class<out Action<*>>, (Action<*>) -> Any> = useCases
    .toMap().mapValues { it.value.toRawInteractor() }