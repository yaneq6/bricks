package io.bricks.interaction.standard.handler

import io.bricks.api.Action
import io.bricks.api.ActionStatus.*
import io.bricks.api.UseCase
import io.reactivex.subjects.PublishSubject
import org.junit.Test

class ActionHandlerTest {

    private val exception = Exception("test exception")
    private val actionHandler = ActionHandlerImpl()
    private val interactor = TestInteractor(exception)

    @Test
    fun `should handle with success`() {
        val action = TestAction(false)
        val statusTest = actionHandler.status.test()
        val actionsTest = interactor.actions.test()

        actionHandler.run { interactor(action) }

        actionsTest.assertValue(action)
        statusTest.assertValues(
            Start(action),
            Success(action),
            Stop(action)
        )
    }

    @Test
    fun `should handle with exception`() {
        val action = TestAction(true)
        val statusTest = actionHandler.status.test()
        val actionsTest = interactor.actions.test()

        actionHandler.run { interactor(action) }

        actionsTest.assertValue(action)
        statusTest.assertValues(
            Start(action),
            Error(action, exception),
            Stop(action)
        )
    }

    class TestAction(val fail: Boolean): Action<Boolean>

    class TestInteractor(val exception: Exception): UseCase<TestAction, Unit> {
        val actions = PublishSubject.create<TestAction>()
        override fun TestAction.execute() {
            actions.onNext(this)
            if(fail) throw exception
        }
    }
}