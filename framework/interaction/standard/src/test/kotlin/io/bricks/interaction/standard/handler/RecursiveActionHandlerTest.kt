package io.bricks.interaction.standard.handler

import io.bricks.api.Action
import io.bricks.api.ActionStatus
import io.bricks.api.ActionStatus.*
import io.bricks.api.UseCase
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertEquals
import org.junit.Test

class RecursiveActionHandlerTest {

    @Test
    fun shouldMapUseCaseToInteractorFunction() {
        assertEquals(0, Interactor(0).toRawInteractor().invoke(IntAction))
    }


    @Test
    fun shouldExecuteProperInteractor() {
        RecursiveActionHandlerImpl(rawInteractors(
            IntAction::class.java to Interactor(0),
            StringAction::class.java to Interactor(""),
            UnitAction::class.java to Interactor(Unit)
        )).run {
            assertEquals(0, IntAction())
        }
    }

    private object IntAction : Action<Int>
    private object StringAction : Action<String>
    private object UnitAction : Action<Unit>

    private class Interactor<R>(
        val result: R
    ) : UseCase<Action<R>, R> {
        override fun Action<R>.execute() = result
    }


    @Test
    fun shouldExecuteInteractorsRecursive() {
        RecursiveActionHandlerImpl(rawInteractors(
            FirstAction::class.java to LambdaInteractor { SecondAction },
            SecondAction::class.java to LambdaInteractor { LastAction },
            LastAction::class.java to LambdaInteractor { "Finish" }
        )).run {
            TestObserver<ActionStatus>()
                .also(status::subscribe)
                .also { assertEquals("Finish", FirstAction()) }
                .assertNoErrors()
                .assertValues(
                    Start(FirstAction),
                    Success(FirstAction),
                    Stop(FirstAction),
                    Start(SecondAction),
                    Success(SecondAction),
                    Stop(SecondAction),
                    Start(LastAction),
                    Success(LastAction),
                    Stop(LastAction)
                )
        }
    }

    private object FirstAction : Action<SecondAction>
    private object SecondAction : Action<LastAction>
    private object LastAction : Action<Unit>

    private class LambdaInteractor<A : Action<R>, R>(
        private val exec: A.() -> R
    ) : UseCase<A, R> {
        override fun A.execute(): R = exec()
    }
}