package io.bricks.interaction.standard.kodein

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import io.bricks.interaction.standard.handler.ActionHandler
import io.bricks.interaction.standard.handler.ActionHandlerImpl

val actionHandlerModule = Kodein.Module("actionHandlerModule") {
    bind<ActionHandler>() with singleton { ActionHandlerImpl() }
}