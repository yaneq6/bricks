package io.bricks.interaction.rx.handler

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val rxObservableHandlerModule = Kodein.Module("rxObservableHandlerModule") {
    bind<RxObservableActionHandler>() with singleton { RxObservableActionHandlerImpl(instance()) }
}