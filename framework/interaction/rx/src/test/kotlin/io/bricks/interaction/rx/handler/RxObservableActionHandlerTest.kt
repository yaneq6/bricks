package io.bricks.interaction.rx.handler

import io.bricks.api.Action
import io.bricks.interaction.rx.interactor.RxUseCase
import io.bricks.rx.core.RxSchedulers
import io.reactivex.Observable
import org.junit.Test

class RxObservableActionHandlerTest {

    data class TestAction(val string: String) : Action<String>

    private val interactor = RxUseCase<TestAction, String> { map(TestAction::string) }

    private val handler = RxObservableActionHandlerImpl(RxSchedulers.dummy())

    @Test
    fun handle(): Unit = handler.run {
        Observable
            .just(TestAction("test"))
            .handleWith(interactor)
    }
}