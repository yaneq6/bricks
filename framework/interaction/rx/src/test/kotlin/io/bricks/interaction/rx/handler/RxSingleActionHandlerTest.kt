package io.bricks.interaction.rx.handler

import io.bricks.api.Action
import io.bricks.api.ActionClass
import io.bricks.api.ActionStatus
import io.bricks.api.ActionStatus.*
import io.bricks.rx.core.RxSchedulers
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers.from
import org.junit.Ignore
import org.junit.Test

@Ignore
class RxSingleActionHandlerTest {

    companion object {

        private val scheduler = from(Runnable::run)

        private val schedulers = RxSchedulers(scheduler, scheduler)

        private val interactors = rxInteractors(
//            javaClass to getHandleInt(1), TODO
//            javaClass to getHandleString("test") TODO
        )

        val actionHandler: RxSingleActionHandler = RxSingleActionHandlerImpl(
            interactors,
            schedulers
        )

        @Suppress("UNCHECKED_CAST")
        private fun rxInteractors(
            vararg interactors: Pair<ActionClass, (Single<out Nothing>) -> Single<out Any>>
        ): Map<ActionClass, RxSingleInteractor> = interactors
            .toMap()
            .mapValues { it.value as RxSingleInteractor }

        private fun getHandleInt(int: Int = 0): (Single<out IntAction>) -> Single<out Int> = { Single.just(int) }
        private fun getHandleString(string: String = ""): (Single<out StringAction>) -> Single<out String> = { Single.just(string) }
    }

    private object IntAction : Action<Int>
    private object StringAction : Action<String>

    @Test
    fun shouldCreateInteractionFromAction() {
        val testObserver = TestObserver<Pair<Int, String>>()

        actionHandler.run {
            Single.zip<Int, String, Pair<Int, String>>(
                IntAction.rx(),
                StringAction.rx(),
                BiFunction(::Pair)
            ).subscribe(testObserver)
        }

        testObserver.run {
            assertNoErrors()
            assertComplete()
            assertValue(1 to "test")
        }
    }

    @Test
    fun shouldInvokeAction() {
        val testObserver = TestObserver<ActionStatus>()

        actionHandler.run {
            status.subscribe(testObserver)
            IntAction()
        }

        testObserver.run {
            assertNoErrors()
            assertValues(
                Start(IntAction),
                Success(IntAction),
                Stop(IntAction)
            )
        }
    }

}