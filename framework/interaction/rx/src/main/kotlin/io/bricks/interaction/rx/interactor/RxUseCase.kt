package io.bricks.interaction.rx.interactor

import io.bricks.api.Action
import io.bricks.api.UseCase
import io.reactivex.Observable

interface RxUseCase<A: Action<R>, R: Any> : UseCase<Observable<A>, Observable<R>>{
    override fun Observable<A>.execute(): Observable<R>

    private class RxInteractor<A: Action<R>, R: Any>(
        val action: Observable<A>.() -> Observable<R>
    ) : RxUseCase<A, R> {

        override fun Observable<A>.execute() = action()
    }


    companion object {
        operator fun <C: Action<R>, R: Any>invoke(
            invoke: Observable<C>.() -> Observable<R>
        ): RxUseCase<C, R> = RxInteractor(invoke)
    }
}