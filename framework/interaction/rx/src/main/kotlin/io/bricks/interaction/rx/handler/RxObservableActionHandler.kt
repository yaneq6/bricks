package io.bricks.interaction.rx.handler

import io.bricks.api.Action
import io.bricks.api.ActionStatus
import io.bricks.api.ActionStatus.*
import io.bricks.interaction.rx.interactor.RxUseCase
import io.bricks.rx.core.RxSchedulers
import io.reactivex.Observable
import io.reactivex.rxkotlin.zipWith
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

interface RxObservableActionHandler {
    val status: Observable<ActionStatus>
    fun <A: Action<R>, R: Any> Observable<A>.handleWith(interactor: RxUseCase<A, R>): Observable<R>
}

class RxObservableActionHandlerImpl(
    schedulers: RxSchedulers
): RxObservableActionHandler {

    private val statusSubject = BehaviorSubject.create<ActionStatus>()!!

    override val status = statusSubject.observeOn(schedulers.mainScheduler)!!

    override fun <A: Action<R>, R: Any> Observable<A>.handleWith(
        interactor: RxUseCase<A, R>
    ): Observable<R> = Executor(
        command = this,
        interactor = interactor,
        subject = statusSubject
    ).execute()

    private class Executor<A: Action<R>, R: Any>(
        val command: Observable<A>,
        val subject: Subject<ActionStatus>,
        interactor: RxUseCase<A, R>
    ) {
        fun execute(): Observable<R> = command
            .notifyStart()
            .zipWith(transfer, addAction)
            .notifyStop()
            .mapToResult()

        private fun Observable<A>.notifyStart(): Observable<A> = doOnNext {
            subject.onNext(Start(it))
        }

        private val transfer: Observable<Transfer<A, R>> = interactor
            .run { command.execute() }
            .map { Transfer<A, R>(result = it) }
            .onErrorReturn { Transfer(error = it) }

        private val addAction = { action: A, transfer: Transfer<A, R> ->
            transfer.copy(action = action)
        }

        private fun Observable<Transfer<A, R>>.notifyStop() = doOnNext { (action, _, error) ->
            action!!
            subject.onNext(
                if (error != null) Error(action, error)
                else Success(action)
            )
            subject.onNext(Stop(action))
        }!!

        private fun Observable<Transfer<A, R>>.mapToResult(): Observable<R> = this
            .map { it.result ?: throw it.error ?: throw IllegalStateException(it.toString()) }

        private data class Transfer<A: Action<R>, R: Any>(
            val action: A? = null,
            val result: R? = null,
            val error: Throwable? = null
        )
    }
}