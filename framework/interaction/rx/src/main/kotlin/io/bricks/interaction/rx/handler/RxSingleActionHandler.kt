package io.bricks.interaction.rx.handler

import io.bricks.api.Action
import io.bricks.api.ActionClass
import io.bricks.api.ActionStatus
import io.bricks.api.ActionStatus.*
import io.bricks.api.UnhandledActionException
import io.bricks.rx.core.CompositeRxComponent
import io.bricks.rx.core.RxComponent
import io.bricks.rx.core.RxSchedulers
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject

typealias RxSingleInteractor = (Single<Action<*>>) -> Single<out Any>

interface RxSingleActionHandler : RxComponent {

    operator fun <A : Action<R>, R : Any> A.invoke() = rx().execute()
    fun <A : Action<R>, R : Any> A.rx(): Single<R>
    fun <R : Any> Single<R>.execute()
    val status: Observable<ActionStatus>

}

class RxSingleActionHandlerImpl(
    private val interactors: Map<ActionClass, RxSingleInteractor>,
    schedulers: RxSchedulers
) : RxSingleActionHandler,
    RxComponent by CompositeRxComponent(schedulers) {

    private val statusSubject = BehaviorSubject.create<ActionStatus>()!!

    override val status = statusSubject.observeOn(schedulers.mainScheduler)!!

    @Suppress("UNCHECKED_CAST")
    override fun <A : Action<R>, R : Any> A.rx(): Single<R> = interactors[javaClass]
        .let { it ?: throw UnhandledActionException(this) }
        .let { it(Single.just(this)) }
        .doOnSubscribe { statusSubject.onNext(Start(this)) }
        .doAfterTerminate { statusSubject.onNext(Stop(this)) }
        .doAfterSuccess { statusSubject.onNext(Success(this)) }
        .doOnError { statusSubject.onNext(Error(this, it)) }
        as Single<R>

    override fun <R : Any> Single<R>.execute() {
        +observeOn(schedulers.mainScheduler).subscribeOn(schedulers.backgroundScheduler).subscribe({},{})
    }

}
