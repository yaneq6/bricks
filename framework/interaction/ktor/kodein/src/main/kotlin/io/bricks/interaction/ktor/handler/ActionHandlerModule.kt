package io.bricks.interaction.ktor.handler

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val actionHandlerModule = Kodein.Module("actionHandlerModule") {
    bind() from singleton { ActionHandler.Feature(instance()) }
    bind() from singleton { ActionHandler(instance(), instance()) }
}