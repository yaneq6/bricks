package io.bricks.interaction.ktor.handler

import io.bricks.api.Action
import io.bricks.api.Interactors
import io.bricks.api.UseCase
import io.bricks.serial.core.JsonSource
import io.bricks.serial.core.StringJsonFactory
import io.bricks.serial.core.invoke
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationFeature
import io.ktor.pipeline.PipelineContext
import io.ktor.request.receiveText
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.util.AttributeKey
import kotlin.reflect.KClass

class ActionHandler(
    private val interactors: Interactors,
    private val jsonFactory: StringJsonFactory
) {

    fun registerIn(routing: Routing) = with(routing) {
        with(application.environment.log) {

            debug("===================================================================")
            debug("Registering handlers: ")
            interactors.forEach { (actionClass, useCase) ->
                val actionName = actionClass.java.name

                debug("-$actionName to ${useCase::class.java.name}")

                post(actionName) { execute(actionClass, useCase) }
            }
            debug("===================================================================")
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.execute(

        actionClass: KClass<*>,
        useCase: UseCase<*, *>

    ) = with(context.application.environment.log) {

        val actionJson = JsonSource(
            actionClass,
            context.receiveText()
        )
        val action: Action<*> = jsonFactory { actionJson.deserialize() }
        debug("action: $action")

        @Suppress("UNCHECKED_CAST")
        val result = (useCase as UseCase<Action<*>, *>).run { action.execute() }!!
        debug("resultType: $result")

        val resultJson = jsonFactory { result.serialize() }

        context.respondText { resultJson.source }
    }

    class Feature(
        private val handler: ActionHandler
    ): ApplicationFeature<Application, Unit, Unit> {

        override val key: AttributeKey<Unit> = AttributeKey("RequestHandler")
        override fun install(pipeline: Application, configure: Unit.() -> Unit) {
            pipeline.routing(handler::registerIn)
        }
    }
}
