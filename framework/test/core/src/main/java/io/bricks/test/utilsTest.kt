package io.bricks.test

import io.reactivex.Observable

fun record(
    vararg observables: Observable<*>
) = mutableListOf<Any?>().also { record ->
    observables.forEachIndexed { index, observable ->
        record.add(null)
        observable.subscribe(
            { record[index] = it },
            { record[index] = it }
        )
    }
}