package io.bricks.test

import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Test

class UtilsTest {

    @Test
    fun shouldRecordOneObservable() {
        val actual = record(
            Observable.just(Unit)
        )

        val expected = listOf(
            Unit
        )

        assertEquals(expected, actual)
    }
}