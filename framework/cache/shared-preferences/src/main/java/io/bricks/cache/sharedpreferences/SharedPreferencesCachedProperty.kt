package io.bricks.cache.sharedpreferences

import android.content.SharedPreferences
import io.bricks.cache.core.CachedProperty
import io.bricks.serial.core.JsonSource
import io.bricks.serial.core.StringJsonFactory
import io.bricks.serial.core.invoke
import kotlin.reflect.KClass

class SharedPreferencesCachedProperty<T: Any>(
    private val sharedPreferences: SharedPreferences,
    private val jsonFactory: StringJsonFactory,
    private val key: String,
    private val type: KClass<T>,
    defaultValue: T? = null
): CachedProperty<T> {

    override var value: T? = defaultValue
        get() {
            return if (field != null) field
            else deserialized.also { field = it }
        }
        set(value) {
            field = value
            deserialized = value
        }

    private var deserialized: T?
        get() = jsonFactory {
            serialized?.let { JsonSource(type, it).deserialize() }
        }
        set(value) {
            serialized = jsonFactory { value?.serialize()?.source }
        }

    private var serialized
        get() = sharedPreferences.getString(key, null)
        set(value) = sharedPreferences.edit().putString(key, value).apply()


    class Factory(
        private val sharedPreferences: (KClass<*>) -> SharedPreferences,
        private val jsonFactory: StringJsonFactory
    ): CachedProperty.Factory {

        override fun <T: Any> create(
            key: String,
            type: KClass<T>
        ) = SharedPreferencesCachedProperty(
            sharedPreferences = sharedPreferences(this::class),
            jsonFactory = jsonFactory,
            key = key,
            type = type
        )
    }
}