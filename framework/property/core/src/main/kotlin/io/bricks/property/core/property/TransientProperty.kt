package io.bricks.property.core.property

import io.reactivex.Observer
import io.reactivex.subjects.PublishSubject

class TransientProperty<T: Any> internal constructor(): SubjectProperty<T, PublishSubject<T>>() {
    override val subject = PublishSubject.create<T>()!!

    override fun subscribeActual(observer: Observer<in T>) {
        subject.subscribeActual(observer)
    }
}