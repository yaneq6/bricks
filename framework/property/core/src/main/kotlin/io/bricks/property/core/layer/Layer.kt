package io.bricks.property.core.layer

import io.bricks.property.core.property.Property
import io.bricks.property.core.property.StateProperty
import io.bricks.property.core.property.TransientProperty
import io.reactivex.disposables.Disposable

abstract class Layer: Disposable {

    private val properties = mutableListOf<Property<*>>()

    private operator fun Property<*>.unaryPlus() = also { properties += it }
    private operator fun Property<*>.unaryMinus() = also { properties -= it }

    protected fun <T: Any> stateProperty() = StateProperty<T>()

    protected fun <T: Any> transientProperty() = TransientProperty<T>()

    override fun isDisposed(): Boolean = properties.isEmpty()

    override fun dispose() {
        properties.apply {
            forEach(Property<*>::dispose)
            clear()
        }
    }
}