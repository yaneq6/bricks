package io.bricks.property.core.binding

import io.bricks.property.core.property.Property
import io.reactivex.Observable

class InverseBinding<T1: Any, T2: Any>(
    private val transform: Observable<T2>.() -> Observable<T1>
): IBinding<T1, T2> {
    override fun bind(property1: Property<T1>, property2: Property<T2>) {
        property2
            .compose(transform)
            .subscribe(
                property1::onNext,
                property1::onError,
                property1::onComplete
            )
            .also(property2::onSubscribe)
    }
}