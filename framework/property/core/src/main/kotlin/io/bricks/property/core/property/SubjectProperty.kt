package io.bricks.property.core.property

import io.reactivex.subjects.Subject

abstract class SubjectProperty<T: Any, S: Subject<T>>: Property<T>() {

    protected abstract val subject: S

    override fun hasThrowable(): Boolean = subject.hasThrowable()

    override fun hasObservers(): Boolean = subject.hasObservers()

    override fun onComplete() = subject.onComplete()

    override fun onError(e: Throwable) = subject.onError(e)

    override fun getThrowable(): Throwable? = subject.throwable

    override fun onNext(t: T) = subject.onNext(t)

    override fun hasComplete(): Boolean = subject.hasComplete()

}