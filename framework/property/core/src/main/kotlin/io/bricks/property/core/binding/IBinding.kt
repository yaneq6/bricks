package io.bricks.property.core.binding

import io.bricks.property.core.property.Property

interface IBinding<T1: Any, T2: Any> {
    fun bind(property1: Property<T1>, property2: Property<T2>)
}