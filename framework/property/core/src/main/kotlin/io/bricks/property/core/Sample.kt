package io.bricks.property.core

import io.bricks.property.core.binding.Binding
import io.bricks.property.core.layer.Layer

private val sample = {

    class State : Layer() {
        val stateProperty = stateProperty<String>()
    }
    class ViewModel : Layer() {
        val transientProperty = transientProperty<String>()
    }


    fun run() {
        val state = State()
        val viewModel = ViewModel()

        val decorateString = Binding<String, String> {
            map { "== $it ==" }
        }

        decorateString.bind(
            viewModel.transientProperty,
            state.stateProperty
        )

    }
}