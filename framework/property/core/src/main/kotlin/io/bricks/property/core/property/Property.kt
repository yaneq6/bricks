package io.bricks.property.core.property

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.Subject

abstract class Property<T: Any>: Subject<T>(), Disposable {

    private val compoundDisposable = CompositeDisposable()

    final override fun onSubscribe(d: Disposable) {
        compoundDisposable += d
    }

    override fun isDisposed(): Boolean = compoundDisposable.isDisposed

    override fun dispose() = compoundDisposable.dispose()
}