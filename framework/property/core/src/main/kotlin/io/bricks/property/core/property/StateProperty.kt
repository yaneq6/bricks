package io.bricks.property.core.property

import io.reactivex.Observer
import io.reactivex.subjects.BehaviorSubject

class StateProperty<T: Any> internal constructor(): SubjectProperty<T, BehaviorSubject<T>>() {
    override val subject = BehaviorSubject.create<T>()!!

    override fun subscribeActual(observer: Observer<in T>?) {
        subject.subscribeWith(observer)
    }
}