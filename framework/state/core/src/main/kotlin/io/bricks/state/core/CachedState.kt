package io.bricks.state.core

import io.bricks.cache.core.CachedProperty

interface CachedState : CachedProperty.Factory

class CachedStateImpl(
    cacheFactory: CachedProperty.Factory
):
    CachedState,
    CachedProperty.Factory by cacheFactory
