package io.bricks.state.core

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val cachedStateModule = Kodein.Module("cachedStateModule") {
    bind<CachedState>() with singleton { CachedStateImpl(instance()) }
}