package io.bricks.kotlin.dsl.base

import java.io.File

abstract class Spec(
    val root: String = ""
) {
    open val rootFile: File by lazy { File(root).absoluteFile }

    open fun build() {}
}
