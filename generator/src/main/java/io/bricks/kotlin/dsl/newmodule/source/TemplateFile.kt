package io.bricks.kotlin.dsl.newmodule.source

import io.bricks.kotlin.dsl.newmodule.File
import io.bricks.kotlin.dsl.newmodule.module.SourceDirectory

abstract class TemplateFile : File() {

    abstract val template: String

    override val file get() = super.file

    val packageName: String get() = (find { it is SourceDirectory } as? SourceDirectory)?.fullPackageName ?: ""

    override fun build(): Unit = with(file) {
        fun print(created: Boolean) = println("[$created] $path")

        when {
            rebuild || createNewFile() -> {
                writeText(template)
                print(true)
            }
            else -> print(false)
        }
    }

    companion object {
        const val TAB = "    "
    }
}