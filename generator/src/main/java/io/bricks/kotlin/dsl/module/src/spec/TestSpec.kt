package io.bricks.kotlin.dsl.module.src.spec

import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.base.KSourceSpec

class TestSpec(
    module: ModuleSpec,
    domain: String
) : KSourceSpec(
    module = module,
    domain = domain,
    type = "Spec",
    flavor = "spec"
) {

    override val template
        get() = """

package $packageName

interface $name {

}

""".trim()

}

fun ModuleSpec.testSpec(name: String = domain) = +TestSpec(
    module = this,
    domain = name
)