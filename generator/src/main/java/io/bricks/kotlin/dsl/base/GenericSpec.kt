package io.bricks.kotlin.dsl.base

import java.io.File

/**
 * outputDir:  " rootFile / domain / dirName / flavour / sourceType / packageName /"
 * exampleDir: "   ./     / module  /   src   /  main   /    java    /   package   /"
 */

open class GenericSpec(
    val domain: String, // filename

    root: String = "",
    group: Boolean = false,
    withType: Boolean = true,

    override val rootFile: File = getRootFile(root),
    protected val dirName: String? = null,
    protected val sourceType: String? = null,
    protected val extension: String? = null,

    packageName: String? = null,
    open val type: String? = null,
    open val flavor: String? = null,
    open val fileName: String = getFileName(domain, getTypeSuffix(withType, type), extension),

    val arguments: Map<String, Any> = emptyMap(),

    outputDir: File? = null,

    outputFile: File? = null
) : Spec(root) {

//    data class Config(
//        val domain: String,
//
//        val root: String = "",
//        val group: Boolean = false,
//        val withType: Boolean = true,
//
//        val rootFile: File = getRootFile(root),
//        val dirName: String? = null,
//        val sourceType: String? = null,
//        val extension: String? = null,
//
//        val packageName: String? = null,
//        val type: String? = null,
//        val flavor: String? = null,
//        val fileName: String = getFileName(domain, getTypeSuffix(withType, type), extension),
//
//        val arguments: Map<String, Any> = emptyMap(),
//
//        val outputDir: File? = null,
//
//        val outputFile: File? = null
//    )

    open val outputDir by lazy {
        outputDir ?: getDirFile(
            rootFile,
            dirName,
            flavor,
            sourceType,
            this.packageName?.path
        )
    }
    open val outputFile by lazy {
        outputFile ?: getFile(
            this.outputDir,
            fileName
        )
    }
    open val packageName = packageName?.let { if (group) "$it.${type!!.toLowerCase()}" else it }
    open val template: String = ""

    override fun build() {
        if (!rootFile.exists()) throw Exception(
            "Cannot create [${outputFile.absolutePath}], root: [${rootFile.absolutePath}] not exist"
        )

        outputDir.mkdirs()

        outputFile.createNewFile()
        outputFile.writeText(template)
    }
}

private val String.path get() = replace('.', '/')

internal fun getGroupPath(group: Boolean, type: String?) = type?.takeIf { group }?.toLowerCase()

internal fun getTypeSuffix(withType: Boolean, type: String?) = type.takeIf { withType } ?: ""

internal fun getRootFile(
    root: String
) = File(root).absoluteFile

internal fun getFileName(
    context: String?,
    type: String?,
    extension: String?
) = "$context$type${extension?.let { ".$it" }}"

internal fun getDirFile(
    rootFile: File,
    vararg chunks: String?
) = rootFile.resolve(chunks.path)

private val Array<out String?>.path
    get() = listOfNotNull(*this)
        .takeIf { it.isNotEmpty() }
        ?.reduce { acc, s -> "$acc/$s" }
        ?: ""

internal fun getFile(
    dir: File,
    fileName: String
) = dir.resolve(fileName)