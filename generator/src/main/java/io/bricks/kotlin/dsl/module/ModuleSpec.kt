package io.bricks.kotlin.dsl.module

import io.bricks.kotlin.dsl.base.GenericSpec
import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.base.Spec

open class ModuleSpec(
    val domain: String,
    val packageName: String,
    val ignoreTests: Boolean = false,
    val specs: MutableSet<Spec> = mutableSetOf(),
    root: String = ""
) : Spec(root) {

    operator fun <T : Spec> T.unaryPlus() = also { specs += it }

    override fun build() {
        rootFile.mkdir()
        specs.forEach(Spec::build)
    }
}

class SubmoduleSpec(
    domain: String,
    packageName: String,
    ignoreTests: Boolean = false,
    specs: MutableSet<Spec> = mutableSetOf(),
    root: String = ""
) : ModuleSpec(
    domain = domain,
    packageName = packageName,
    ignoreTests = ignoreTests,
    specs = specs,
    root = root
) {
    override fun build() {
        println(rootFile.path)
        println(rootFile.canonicalPath)
        rootFile.mkdir()
    }
}

inline fun <reified T : KSourceSpec?> ModuleSpec.find(
    name: String?,
    create: (String) -> T = { null as T }
): T = specs.find { it is T && it.domain == name } as? T ?: name?.let(create) as T

inline fun <reified T : Spec> ModuleSpec.find() = specs.filter { it is T }.map { it as T }

inline fun <reified T : Spec> ModuleSpec.find(predicate: (T) -> Boolean) = find<T>().filter(predicate)

val <T : GenericSpec>Class<T>.rawName: String get() = simpleName.removeSuffix("Spec")

inline fun <reified T : GenericSpec> ModuleSpec.findByType() = find<T> {
    it.type == T::class.java.rawName
}.takeNotEmpty()

fun <T : Spec> List<T>.takeNotEmpty() = takeIf { it.isNotEmpty() }