package io.bricks.kotlin.dsl.newmodule.source.gradle

import io.bricks.kotlin.dsl.newmodule.Dir
import io.bricks.kotlin.dsl.newmodule.module.Mod
import io.bricks.kotlin.dsl.newmodule.source.GradleFile

class SettingsGradle: GradleFile() {

    override var name: String = "settings.gradle"

    init {
        rebuild = true
    }

    override val template: String
        get() {
            val names = mutableSetOf<String>()
            val rest = mutableListOf<String>()

            val modules = parent!!.findModules()

            names += modules.map { it.moduleName }

            file.forEachLine {
                if (it.startsWith("include")) names += it.split("'")[1]
                else if (it.isNotBlank()) rest += it + "\n"
            }

            return (rest + names.map {
                "include '$it'\n"
            }).sorted().reduce { acc, s ->
                acc + s
            }.also(::print)
        }

    private fun Dir.findModules() = mutableSetOf<Mod>()
        .also { appendModules(it) }
        .toSet()

    private fun Dir.appendModules(
        modules: MutableSet<Mod>
    ) {
        files.forEach {
            if (it is Mod) modules += it
            if (it is Dir) it.appendModules(modules)
        }
    }
}