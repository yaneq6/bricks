package io.bricks.kotlin.dsl.module.src.main

import io.bricks.kotlin.dsl.base.GenericSpec
import io.bricks.kotlin.dsl.module.ModuleSpec

open class AndroidManifestSpec(
    module: ModuleSpec,
    flavour: String
) : GenericSpec(
    domain = module.domain,
    root = module.root,
    dirName = "src",
    flavor = flavour,
    fileName = "AndroidManifest.xml"
) {

    override val packageName = ""
    private val internalPackageName = module.packageName
    override val template get() = """

<manifest package="$internalPackageName" />

""".trim()

}

fun ModuleSpec.androidManifest(
    flavour: String = "main"
) = +AndroidManifestSpec(
    module = this,
    flavour = flavour
)