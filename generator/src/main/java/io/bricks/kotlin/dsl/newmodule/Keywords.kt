package io.bricks.kotlin.dsl.newmodule

const val packagePrefix = "packagePrefix"
const val androidTest = "androidTest"
const val test = "test"
const val main = "main"

const val framework = "framework"
const val core = "core"
const val android = "android"
const val ui = "ui"
const val model = "model"
const val standard = "standard"
