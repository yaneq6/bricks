package io.bricks.kotlin.dsl.module.src.main

import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.base.import
import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.module.findByType
import io.bricks.kotlin.dsl.module.toString

class KodeinModuleSpec(
    module: ModuleSpec,
    domain: String
) : KSourceSpec(
    module,
    domain,
    type = "Module"
) {

    private val interactors by lazy { module.findByType<InteractorSpec>() }

    private val viewModels by lazy { module.findByType<ViewModelSpec>() }

    private val states by lazy { module.findByType<StateSpec>() }

    private val interactorsTemplate
        get() = interactors.toString { "    bindInteractor { ${it.name}(instance(), instance()) }" }

    private val viewModelsTemplate
        get() = viewModels.toString { "    bindViewModel<${it.name}>()" }

    private val statesTemplates
        get() = states.toString { "    bind() from singleton { ${it.name}() }" }

    override val template
        get() = """

package $packageName

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import io.bricks.android.kodein.bindViewModel
import io.bricks.api.toRawInteractor.bindInteractor
import io.bricks.api.toRawInteractor.bindInteractors
${import(interactors)}

val ${name.decapitalize()} = Kodein.Module("${name.decapitalize()}") {
    bindInteractors()
$statesTemplates
$interactorsTemplate
$viewModelsTemplate
}

""".trim()

}

fun ModuleSpec.kodeinModule(
    name: String = domain
) = +KodeinModuleSpec(
    module = this,
    domain = name
)