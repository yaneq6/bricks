package io.bricks.kotlin.dsl.newmodule.module

import io.bricks.kotlin.dsl.newmodule.main
import io.bricks.kotlin.dsl.newmodule.source.gradle.JavaGradle
import io.bricks.kotlin.dsl.newmodule.test

open class JavaModule internal constructor(): SourceModule<JavaGradle>() {

    final override val buildGradle by lazy { file<JavaGradle>() }

    override val src by lazy {
        src(main, test) {
            type = "kotlin"
        }
    }
}