package io.bricks.kotlin.dsl.module.src.main

import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.module.find

class ActivitySpec(
    module: ModuleSpec,
    domain: String,
    val viewModel: ViewModelSpec,
    val layout: LayoutSpec
) : KSourceSpec(
    module = module,
    domain = domain,
    type = "Activity"
) {

    override val template
        get() = """

package $packageName

import android.os.Bundle
import org.kodein.di.Kodein
import org.kodein.di.generic.kodein
import io.bricks.android.base.BaseActivity
import io.bricks.android.base.controllerModule
import io.bricks.android.base.schedulersModule
import io.bricks.android.kodein.KodeinViewModelFactory
import io.bricks.android.kodein.provideViewModel

class $name : BaseActivity() {

    private val viewModel by lazy {
        provideViewModel<${viewModel.name}>(KodeinViewModelFactory(kodein().value))
    }
//    private val viewModel: ${viewModel.name} by instance()

    override fun provideOverridingModule() = Kodein.Module("$name") {
//        import(repositoryModule)
        import(schedulersModule)
        import(controllerModule)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.${layout.name})
    }

}

""".trim()

}

fun ModuleSpec.activity(
    name: String = domain
): ActivitySpec = +ActivitySpec(
    module = this,
    domain = name,
    viewModel = find(name, ::viewModel),
    layout = layout("activity")
)

fun ModuleSpec.activities(vararg names: String) = names.map(::activity)