package io.bricks.kotlin.dsl.newmodule.source.gradle

import io.bricks.kotlin.dsl.newmodule.source.GradleFile

class AndroidGradle: GradleFile() {
    override val template
        get() = """

apply plugin: 'com.android.library'
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-kapt'
apply plugin: 'kotlin-android-extensions'
android {
    defaultConfig {
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"

    }
    lintOptions {
        abortOnError false
    }
    androidExtensions {
        experimental = true
    }
}
dependencies {
    implementation dep.kotlin
    implementation project(':framework:android:core')
    implementation project(':framework:android:core:kodein')
    testImplementation project(':framework:test:core')
    androidTestImplementation project(':framework:android:test')
}

""".trim()
}