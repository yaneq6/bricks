package io.bricks.kotlin.dsl.module

import io.bricks.kotlin.dsl.base.Spec
import io.bricks.kotlin.dsl.module.src.androidTest.activityTest
import io.bricks.kotlin.dsl.module.src.androidTest.androidTestManifest
import io.bricks.kotlin.dsl.module.src.androidTest.testApplication
import io.bricks.kotlin.dsl.module.src.main.*
import io.bricks.kotlin.dsl.module.src.repoImpl.repositoryImpl
import io.bricks.kotlin.dsl.module.src.repoMock.repositoryMock
import io.bricks.kotlin.dsl.module.src.spec.testSpec
import io.bricks.kotlin.dsl.module.src.test.interactorTest
import io.bricks.kotlin.dsl.module.src.test.viewModelTest

fun application(
    packageName: String,
    ignoreTests: Boolean = false,
    init: ModuleSpec.() -> Unit
) = ModuleSpec(
    packageName = packageName,
    domain = "application",
    ignoreTests = ignoreTests
).also(init)

fun ModuleSpec.feature(
    domain: String,
    ignoreTests: Boolean = this.ignoreTests,
    init: ModuleSpec.() -> Unit
) = subModule(
    context = domain,
    domain = domain,
    specs = specs,
    ignoreTests = ignoreTests,
    init = init
)

fun ModuleSpec.android(
    ignoreTests: Boolean = this.ignoreTests,
    init: ModuleSpec.() -> Unit
) = subModule(
    context = "android",
    specs = specs,
    ignoreTests = ignoreTests
) {
    androidGradle()
    init()
}

fun ModuleSpec.api(
    ignoreTests: Boolean = this.ignoreTests,
    init: ModuleSpec.() -> Unit
) = subModule(
    context = "api",
    specs = specs,
    ignoreTests = ignoreTests
) {
    init()
    apiGradle()
}

fun ModuleSpec.backend(
    ignoreTests: Boolean = this.ignoreTests,
    init: ModuleSpec.() -> Unit
) = subModule(
    context = "backend",
    specs = specs,
    ignoreTests = ignoreTests
) {
    //  ktorGradle()
    init()
}

fun ModuleSpec.subModule(
    context: String,
    domain: String = this.domain,
    specs: MutableSet<Spec> = this.specs,
    ignoreTests: Boolean = this.ignoreTests,
    init: ModuleSpec.() -> Unit
) = SubmoduleSpec(
    root = listOf(root, context).filter(String::isNotEmpty).reduce { acc, s -> "$acc/$s" },
    domain = domain,
    packageName = "$packageName.$context",
    ignoreTests = ignoreTests,
    specs = specs
).unaryPlus().also(init)

fun ModuleSpec.core(name: String = domain) {
    androidManifest()

    testSpec(name)

    viewModel(name)
    viewModelTest(name)

    activity(name)
    activityTest(name)

    testApplication(name)
    androidTestManifest(name)

    kodeinModule(name)
}

fun ModuleSpec.useCase(
    name: String
) {
    interactor(name)
    interactorTest(name)
}

fun ModuleSpec.repository(
    name: String = domain
) {
    repositoryImpl(name)
    repositoryMock(name)
}
