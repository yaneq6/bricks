package io.bricks.kotlin.dsl.module.src.main

import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.base.import
import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.module.find

class InteractorSpec(
    module: ModuleSpec,
    domain: String,
    val action: KSourceSpec,
    val state: KSourceSpec,
    val result: KSourceSpec?
) : KSourceSpec(
    module = module,
    domain = domain,
    type = "Interactor",
    group = true
) {

    private val resultName = result?.name ?: "Unit"

    override val template
        get() = """

package $packageName

import io.bricks.android.base.Repository
import io.bricks.api.interactor.UseCase
${import(action)}
${import(result)}
${import(state)}

class $name(
    private val repository: Repository,
    private val state: ${state.name}
) : UseCase<${action.name}, $resultName> {

    override fun ${action.name}.execute(): $resultName = TODO("not implemented")
}

""".trim()

}

fun ModuleSpec.interactor(
    name: String = domain,
    stateName: String = domain,
    action: ActionSpec = find(name) { action(it) }
) = +InteractorSpec(
    module = this,
    domain = name,
    result = action.result,
    action = action,
    state = find(stateName, ::state)
)

fun ModuleSpec.interactors(vararg names: String) = names.map {
    interactor(it, it)
}