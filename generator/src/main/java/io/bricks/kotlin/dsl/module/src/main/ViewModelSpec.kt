package io.bricks.kotlin.dsl.module.src.main

import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.module.find

class ViewModelSpec(
    module: ModuleSpec,
    domain: String,
    val state: StateSpec
) : KSourceSpec(
    module = module,
    domain = domain,
    type = "ViewModel"
) {

    override val template
        get() = """

package $packageName

import android.arch.lifecycle.ViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import io.bricks.android.base.Controller

class $name(
    override val kodein: Kodein
) : ViewModel(),
    KodeinAware,
    Controller by kodein.instance() {

    private val state = instance<${state.name}>()

}

""".trim()

}

fun ModuleSpec.viewModel(
    name: String = domain
) = +ViewModelSpec(
    module = this,
    domain = name,
    state = find(name, ::state)
)

fun ModuleSpec.viewModels(vararg names: String) = names.map(this::viewModel)