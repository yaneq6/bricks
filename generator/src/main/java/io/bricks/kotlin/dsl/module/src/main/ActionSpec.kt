package io.bricks.kotlin.dsl.module.src.main

import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.module.find

class ActionSpec(
    module: ModuleSpec,
    domain: String,
    val result: KSourceSpec?,
    val args: Map<String, KSourceSpec>
) : KSourceSpec(
    module = module,
    domain = domain,
    group = true,
    withType = false,
    type = "Action"
) {

    constructor(
        module: ModuleSpec,
        context: String,
        result: KSourceSpec?,
        vararg args: KSourceSpec
    ) : this(
        module = module,
        domain = context,
        result = result,
        args = args.map { it.name.decapitalize() to it }.toMap()
    )

    override val template
        get() = """

package $packageName

import io.bricks.api.Action
${result?.run { "import $fullName" } ?: ""}

$modifier $name$constructor : Action<$resultName>

""".trim()

    private val resultName = result?.name ?: "Unit"
    private val tab = "    "
    private val ActionSpec.modifier get() = if (args.isEmpty()) "object" else "data class"
    private val Map.Entry<String, KSourceSpec>.arg: String get() = "$key: ${value.name}"
    private fun Map<String, KSourceSpec>.print() = map { "val ${it.arg}" }.reduce { acc, s -> "$acc,\n$tab$s" }
    private val ActionSpec.constructor get() = if (args.isEmpty()) "" else "(\n$tab${args.print()}\n)".trim()
}

fun ModuleSpec.action(
    name: String = domain,
    result: String? = null,
    vararg args: KSourceSpec
) = +ActionSpec(
    module = this,
    context = name,
    result = find<KSourceSpec?>(result) { null },
    args = *args
)