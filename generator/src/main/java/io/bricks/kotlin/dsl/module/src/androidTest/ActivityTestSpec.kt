package io.bricks.kotlin.dsl.module.src.androidTest

import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.base.test
import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.module.find
import io.bricks.kotlin.dsl.module.src.main.ActivitySpec
import io.bricks.kotlin.dsl.module.src.main.activity
import io.bricks.kotlin.dsl.module.src.spec.TestSpec
import io.bricks.kotlin.dsl.module.src.spec.testSpec

class ActivityTestSpec(
    module: ModuleSpec,
    domain: String,
    val spec: TestSpec,
    val activity: ActivitySpec
) : KSourceSpec(
    module,
    domain,
    type = "ActivityTest",
    flavor = "androidTest"
) {

    override val template
        get() = """
package $packageName

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import org.kodein.di.generic.kodein
import io.bricks.android.base.BaseActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.Ignore
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class $name : ${spec.name},
                          KodeinAware {

    @Rule
    @JvmField
    val rule = ActivityTestRule(${activity.name}::class.java)

    override val kodein: Kodein get() = (rule.activity as BaseActivity).kodein().value

    @Before
    fun setUp() {

    }

${test()}
    fun test() {

    }
}
""".trim()
}

fun ModuleSpec.activityTest(
    name: String = domain,
    activity: ActivitySpec = find(name, ::activity),
    spec: TestSpec = find(name, ::testSpec)
): ActivityTestSpec = +ActivityTestSpec(
    module = this,
    domain = name,
    spec = spec,
    activity = activity
)