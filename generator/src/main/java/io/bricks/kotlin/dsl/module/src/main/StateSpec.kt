package io.bricks.kotlin.dsl.module.src.main

import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.module.find
import io.bricks.kotlin.dsl.module.takeNotEmpty

class StateSpec(
    module: ModuleSpec,
    domain: String
) : KSourceSpec(
    module = module,
    domain = domain,
    type = "State"
) {
    override val template
        get() = """

package $packageName

class $name

""".trim()

}

fun ModuleSpec.state(
    name: String = domain
) = +StateSpec(
    module = this,
    domain = name
)

fun ModuleSpec.states(vararg names: String) = names.map(this::state)

fun ModuleSpec.getStates() = find<StateSpec> { it.type == "State" }.takeNotEmpty()