package io.bricks.kotlin.dsl.module

import io.bricks.kotlin.dsl.base.GenericSpec

open class AndroidGradleSpec(
    root: String,
    domain: String
) : GenericSpec(
    root = root,
    domain = domain,
    fileName = "build.gradle",
    extension = "gradle"
) {
    override val template get() = """
apply plugin: 'com.android.library'
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-kapt'
apply plugin: 'kotlin-android-extensions'
android {
    defaultConfig {
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"

    }
    flavorDimensions "repo"
    productFlavors {
        repoImpl {
            dimension "repo"
        }
        repoMock {
            dimension "repo"
        }
    }
    lintOptions {
        abortOnError false
    }
    sourceSets {
        test.java.srcDirs += 'src/spec/java'
        androidTest.java.srcDirs += 'src/spec/java'
    }
    androidExtensions {
        experimental = true
    }
}
dependencies {
    implementation dep.kotlin
    implementation project(':framework:android:core')
    implementation project(':framework:android:core:kodein')
    testImplementation project(':framework:base-test')
    testImplementation(dep.androidArchTest, {
        exclude group: 'com.android.support', module: 'support-compat'
        exclude group: 'com.android.support', module: 'support-annotations'
        exclude group: 'com.android.support', module: 'support-core-utils'
    })
    androidTestImplementation dep.androidTest
    androidTestImplementation project(':framework:base-android-test')
    kapt dep.androidArchCompiler
}

""".trim()

}

fun ModuleSpec.androidGradle() = +AndroidGradleSpec(root, domain)