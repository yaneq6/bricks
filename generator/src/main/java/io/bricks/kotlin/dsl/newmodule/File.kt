package io.bricks.kotlin.dsl.newmodule

open class File internal constructor(): Spec, Iterable<File> {
    override lateinit var name: String

    protected val compoundConfig: CompoundMap<String, Any?> = CompoundMap()

    val config by lazy { compoundConfig.withDefault { null } }

    var rebuild: Boolean by config

    open var parent: Directory? = null
        set(value) {
            if (field == value) return

            field?.files?.remove(this)
            field?.compoundConfig?.parent = null

            field = value?.also {
                it.files.add(this@File)
                compoundConfig.parent = it.compoundConfig
            }
        }

    protected open val file get() = fileName.file

    open val fileName get() = asSequence().getStringPath()

    override fun match(vararg path: String): File? =
        if (path.size == 1 && path.first() == name) this
        else null

    override fun iterator() = object: Iterator<File> {
        var next: File? = this@File
        override fun hasNext() = next != null
        override fun next(): File = next!!.apply { next = parent }
    }

    fun Sequence<File>.getStringPath(delimiter: String = "/") = map(File::name).reduce(toPath(delimiter))

    fun toPath(delimiter: String) = { path: String, name: String ->
        if (name.isNotBlank()) "$name$delimiter$path"
        else path
    }

    override fun build(): Unit = with(file) {
        if (rebuild) delete()
        val created = createNewFile()
        println("[$created] $path")
    }

    internal open fun selfInit() {}

    override fun toString() = fileName
}