package io.bricks.kotlin.dsl.newmodule.source.code

import io.bricks.kotlin.dsl.newmodule.source.SourceFile

class KotlinFile : SourceFile() {

    override val fileName: String
        get() = super.fileName + ".kt"

}

typealias KtFile = KotlinFile