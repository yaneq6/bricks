package io.bricks.kotlin.dsl.module.src.test

import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.base.test
import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.module.find
import io.bricks.kotlin.dsl.module.src.main.KodeinModuleSpec
import io.bricks.kotlin.dsl.module.src.main.ViewModelSpec
import io.bricks.kotlin.dsl.module.src.main.kodeinModule
import io.bricks.kotlin.dsl.module.src.main.viewModel
import io.bricks.kotlin.dsl.module.src.repoMock.RepositoryMockSpec
import io.bricks.kotlin.dsl.module.src.repoMock.repositoryMock
import io.bricks.kotlin.dsl.module.src.spec.TestSpec
import io.bricks.kotlin.dsl.module.src.spec.testSpec

class ViewModelTestSpec(
    module: ModuleSpec,
    domain: String,
    val spec: TestSpec,
    val viewModel: ViewModelSpec,
    val kodeinModule: KodeinModuleSpec,
    val repository: RepositoryMockSpec
) : KSourceSpec(
    module,
    domain,
    type = "ViewModelTest",
    flavor = "test"
) {

    override val template
        get() = """

package $packageName

import org.kodein.di.Kodein
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.Ignore

class $name : ${spec.name} {

    lateinit var viewModel: ${viewModel.name}

    @Before
    fun setup() {
        val kodein = Kodein {
            import(${repository.moduleName})
            import(${kodeinModule.name.decapitalize()})
        }
        viewModel = ${viewModel.name}(kodein)
    }

${test()}
    fun test() {
        TODO()
    }

}

""".trim()
}

fun ModuleSpec.viewModelTest(
    name: String = domain,
    spec: TestSpec = find(name, ::testSpec),
    viewModel: ViewModelSpec = find(name, ::viewModel),
    kodeinModule: KodeinModuleSpec = find(name, ::kodeinModule),
    repository: RepositoryMockSpec = find(name, ::repositoryMock)
) = +ViewModelTestSpec(
    module = this,
    domain = name,
    spec = spec,
    viewModel = viewModel,
    kodeinModule = kodeinModule,
    repository = repository
)