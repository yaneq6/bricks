package io.bricks.kotlin.dsl.base

import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.module.toString

open class KSourceSpec(
    val module: ModuleSpec,
    domain: String,
    type: String,
    flavor: String = "main",
    packageName: String = module.packageName,
    group: Boolean = false,
    val withType: Boolean = true
) : GenericSpec(
    root = module.root,
    dirName = "src",
    flavor = flavor,
    sourceType = "java",
    packageName = packageName,
    domain = domain,
    type = type,
    extension = "kt",
    group = group,
    withType = withType
) {
    open val name get() = "$domain${getTypeSuffix(withType, type)}"
    val fullName get() = "$packageName.$name"
}

fun KSourceSpec.import(
    vararg dependency: KSourceSpec?
): String = import(dependency.toList().filterNotNull())

fun KSourceSpec.import(
    dependencies: List<KSourceSpec>?
): String = dependencies?.toList().toString { "import ${it.fullName}" }

fun <T: Any>arguments(
    vararg args: T?,
    transform: T.() -> String
) = args
    .filterNotNull()
    .toString(transform)

private const val tab = "    "
fun KSourceSpec.tab(number: Int = 1) = (1..number).map { tab }.reduce { acc, s -> acc+s }

fun KSourceSpec.test(
    ignore: Boolean = module.ignoreTests
) = "${tab()}@Test".let { if (ignore) "$it\n${tab()}@Ignore" else it }