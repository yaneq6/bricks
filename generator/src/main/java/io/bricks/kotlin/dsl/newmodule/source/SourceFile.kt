package io.bricks.kotlin.dsl.newmodule.source

import io.bricks.kotlin.dsl.newmodule.module.Flavour

open class SourceFile: TemplateFile() {

    override val template: String = ""

    override var name: String = ""
        set(value) { field = value.capitalize() }

    val className: String
        get() = asSequence()
            .takeWhile { it !is Flavour }
            .toList().dropLast(1).asSequence()
            .getStringPath().replace("/", ".")
}

