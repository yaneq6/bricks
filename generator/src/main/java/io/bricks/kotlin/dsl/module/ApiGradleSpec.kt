package io.bricks.kotlin.dsl.module

import io.bricks.kotlin.dsl.base.GenericSpec

open class ApiGradleSpec(
    root: String,
    domain: String
) : GenericSpec(
    root = root,
    domain = domain,
    fileName = "build.gradle",
    extension = "gradle"
) {
    override val template get() = """
apply plugin: 'java-library'
apply plugin: 'kotlin'
dependencies {
    implementation dep.kotlin
    api project(':framework:api:core')
}
sourceCompatibility = "1.8"
targetCompatibility = "1.8"
""".trim()

}

fun ModuleSpec.apiGradle() = +ApiGradleSpec(root, domain)