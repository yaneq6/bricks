package io.bricks.kotlin.dsl.module.src.repoMock

import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.base.import
import io.bricks.kotlin.dsl.base.tab
import io.bricks.kotlin.dsl.module.src.main.ActionSpec
import io.bricks.kotlin.dsl.module.toString

class RepositoryMockSpec(
    module: ModuleSpec,
    domain: String
) : KSourceSpec(
    module = module,
    domain = domain,
    type = "Repository",
    flavor = "repoMock"
) {
    private val actions = module.specs
        .filter { it is ActionSpec }
        .map { it as ActionSpec }

    private fun case(action: ActionSpec) = "${tab(2)}is ${action.name} -> TODO()"

    val moduleName = name.decapitalize() + "Module"

    override val template
        get() = """
package $packageName

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import io.bricks.android.base.Repository
import io.bricks.api.Action
${import(actions)}

class ${name}Mock : Repository {

    @Suppress("IMPLICIT_CAST_TO_ANY", "UNCHECKED_CAST")
    override fun <A : Action<R>, R> dispatch(action: A): R = when (action) {
${actions.toString(this::case)}
        else -> throw Exception("Unhandled action exception: ${'$'}action")
    } as R
}

val $moduleName = Kodein.Module("$moduleName") {
    bind() from singleton<Repository> { ${name}Mock() }
}

""".trim()
}

fun ModuleSpec.repositoryMock(name: String = domain) = +RepositoryMockSpec(
    module = this,
    domain = name
)
