package io.bricks.kotlin.dsl.module.src.main

import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.module.ModuleSpec

class EntitySpec(
    module: ModuleSpec,
    domain: String
) : KSourceSpec(
    module,
    domain = domain,
    type = "",
    packageName = "${module.packageName}.entity"
) {

    override val name: String get() = domain
    override val type: String = "Entity"

    override val template
        get() = """

package $packageName

data class $name(val name: String)

""".trim()

}

fun ModuleSpec.entity(
    name: String = domain
) = +EntitySpec(
    module = this,
    domain = name
)

fun ModuleSpec.entities(vararg names: String) = names.map(this::entity)