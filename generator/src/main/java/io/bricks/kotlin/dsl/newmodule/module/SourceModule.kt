package io.bricks.kotlin.dsl.newmodule.module

import io.bricks.kotlin.dsl.newmodule.source.GradleFile

abstract class SourceModule<Gradle: GradleFile> internal constructor(): Module() {
    abstract val buildGradle: Gradle
    abstract val src: SourceDirectory

    override fun build() {
        buildGradle
        src
        super.build()
    }
}