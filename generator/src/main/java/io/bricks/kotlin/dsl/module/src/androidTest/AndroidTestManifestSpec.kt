package io.bricks.kotlin.dsl.module.src.androidTest

import io.bricks.kotlin.dsl.base.GenericSpec
import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.module.find
import io.bricks.kotlin.dsl.module.src.main.ActivitySpec
import io.bricks.kotlin.dsl.module.src.main.activity

class AndroidTestManifestSpec(
    module: ModuleSpec,
    val application: TestApplicationSpec,
    private val activity: ActivitySpec
) : GenericSpec(
    domain = module.domain,
    root = module.root,
    dirName = "src",
    flavor = "androidTest",
    fileName = "AndroidManifest.xml"
) {

    override val packageName = ""
    private val internalPackageName = module.packageName
    override val template get() = """

<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="$internalPackageName" >
    <application
        android:name="${application.fullName}"
        android:theme="@style/Theme.AppCompat.Light.DarkActionBar">
        <activity android:name="${activity.fullName}" />
    </application>
</manifest>

""".trim()

}

fun ModuleSpec.androidTestManifest(
    name: String = domain,
    application: TestApplicationSpec = find(name, ::testApplication),
    activity: ActivitySpec = find(name, ::activity)
) = +AndroidTestManifestSpec(
    module = this,
    application = application,
    activity = activity
)