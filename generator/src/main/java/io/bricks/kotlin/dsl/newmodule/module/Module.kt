package io.bricks.kotlin.dsl.newmodule.module

import io.bricks.kotlin.dsl.newmodule.Directory
import io.bricks.kotlin.dsl.newmodule.File

open class Module internal constructor(): Directory() {

    val moduleName: String
        get() = map { it.name }.reduceRight { s, acc -> "$acc:$s" }

    override fun toString() = moduleName
}

typealias Mod = Module

val File.parentModule: Module get() = parent as? Module ?: throw IllegalStateException("$parent of $this is not ${Module::class}")