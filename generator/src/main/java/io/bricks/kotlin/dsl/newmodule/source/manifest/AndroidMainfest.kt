package io.bricks.kotlin.dsl.newmodule.source.manifest

import io.bricks.kotlin.dsl.newmodule.source.TemplateFile


class AndroidManifest: TemplateFile() {
    override var name: String = "AndroidManifest.xml"

    override val template
        get() = """

<manifest package="$packageName" />

""".trim()
}