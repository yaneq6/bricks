package io.bricks.kotlin.dsl.module

fun <T>Collection<T>?.toString(transform: (T) -> String) = this?.map(transform).toMultilineString()

fun Collection<String>?.toMultilineString() = takeIfNotEmpty()?.reduce { acc, s -> "$acc\n$s" } ?: ""

fun <T>Collection<T>?.takeIfNotEmpty() = this?.takeIf { it.isNotEmpty() }
