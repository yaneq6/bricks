package io.bricks.kotlin.dsl.newmodule.source

abstract class GradleFile: TemplateFile() {
    override var name: String = "build.gradle"
    val dependencies = mutableSetOf<Any>()

    fun dependencies(vararg dependencies: String) {
        this.dependencies.addAll(dependencies)
    }

    protected fun printDependencies() =
        if (dependencies.isEmpty()) ""
        else TAB + dependencies.map(Any::toString)
            .reduce { acc, dep -> "$acc\n$TAB$dep" }
}