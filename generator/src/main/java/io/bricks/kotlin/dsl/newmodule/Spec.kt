package io.bricks.kotlin.dsl.newmodule

import java.io.File

interface Spec {
    var name: String

    fun build()

    val String.file get() = File(this)

    fun match(vararg path: String): Spec?
}