package io.bricks.kotlin.dsl.newmodule

class CompoundMap<K, V>: MutableMap<K, V>, Map<K, V> {

    var parent: CompoundMap<K, V>? = null
        internal set

    private val local = mutableMapOf<K, V>()

    override val size: Int get() = local.size + (parent?.size ?: 0)

    override fun containsKey(key: K): Boolean = local.containsKey(key) || parent?.containsKey(key) ?: false

    override fun containsValue(value: V): Boolean = local.containsValue(value) || parent?.containsValue(value) ?: false

    override fun get(key: K): V? = local[key] ?: parent?.get(key)

    override fun isEmpty(): Boolean = local.isEmpty() && parent?.isEmpty() ?: true

    override val entries: MutableSet<MutableMap.MutableEntry<K, V>> get() = local.entries

    override val keys: MutableSet<K> get() = local.keys

    override val values: MutableCollection<V> get() = local.values

    override fun clear() = local.clear()

    override fun put(key: K, value: V): V? = local.put(key, value)

    override fun putAll(from: Map<out K, V>) = local.putAll(from)

    override fun remove(key: K): V? = local.remove(key)
}