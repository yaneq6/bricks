package io.bricks.kotlin.dsl.module.src.androidTest

import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.base.KSourceSpec

class TestApplicationSpec(
    module: ModuleSpec,
    domain: String
) : KSourceSpec(
    module,
    domain,
    type = "Application",
    flavor = "androidTest") {

    override val template
        get() = """
package $packageName

import android.app.Application
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware

class $name : Application(), KodeinAware {

    override val kodein = Kodein {}

}
""".trim()

}

fun ModuleSpec.testApplication(
    name: String = domain
) = +TestApplicationSpec(
    module = this,
    domain = name
)