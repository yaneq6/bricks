package io.bricks.kotlin.dsl.newmodule.module

import io.bricks.kotlin.dsl.newmodule.source.GradleFile

open class JavaKodeinModule: JavaModule() {
    override var name = "kodein"
    init {
        buildGradle.apply { dependencies.addAll(kodeinDependencies) }
    }
}
open class AndroidKodeinModule: AndroidModule() {
    override var name = "kodein"
    init {
        buildGradle.apply { dependencies.addAll(kodeinDependencies) }
    }
}

private val GradleFile.kodeinDependencies get() = listOf(
    "api dep.kodein",
    dynamic { "api project('${parentModule.parentModule.moduleName}')" }
)

class DynamicString(
    private val provide: () -> String
) : CharSequence {

    override val length: Int get() = toString().length

    override fun toString() = provide()

    override fun get(index: Int): Char = toString().get(index)

    override fun subSequence(
        startIndex: Int,
        endIndex: Int
    ): CharSequence = toString().subSequence(startIndex, endIndex)
}

typealias dynamic = DynamicString