package io.bricks.kotlin.dsl.module.src.repoImpl

import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.base.import
import io.bricks.kotlin.dsl.module.src.main.ActionSpec

class RepositoryImplSpec(
    module: ModuleSpec,
    domain: String
) : KSourceSpec(
    module = module,
    domain = domain,
    type = "Repository",
    flavor = "repoImpl"
) {

    private val actions = module.specs
        .filter { it is ActionSpec }
        .map { it as ActionSpec }

    private fun case(action: ActionSpec) = "is ${action.name} -> TODO()"

    override val template
        get() = """

package $packageName

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import io.bricks.android.base.Repository
import io.bricks.api.Action
${import(actions)}

class ${name}Impl : Repository {

    @Suppress("IMPLICIT_CAST_TO_ANY", "UNCHECKED_CAST")
    override fun <A : Action<R>, R> dispatch(action: A): R = when (action) {
        ${actions.map { case(it) }.takeIf { it.isNotEmpty() }?.reduce { acc, _ -> "$acc\n" } ?: ""}
        else -> throw Exception("Unhandled action exception: ${'$'}action")
    } as R
}

val ${name.decapitalize()}Module = Kodein.Module("${name.decapitalize()}") {
    bind() from singleton<Repository> { ${name}Impl() }
}

""".trim()
}

fun ModuleSpec.repositoryImpl(name: String = domain) = +RepositoryImplSpec(
    module = this,
    domain = name
)
