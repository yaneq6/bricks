package io.bricks.kotlin.dsl.newmodule.module

import io.bricks.kotlin.dsl.newmodule.androidTest
import io.bricks.kotlin.dsl.newmodule.main
import io.bricks.kotlin.dsl.newmodule.source.gradle.AndroidGradle
import io.bricks.kotlin.dsl.newmodule.source.manifest.AndroidManifest
import io.bricks.kotlin.dsl.newmodule.test

open class AndroidModule internal constructor(): SourceModule<AndroidGradle>() {

    final override val buildGradle by lazy { file<AndroidGradle>() }

    override val src by lazy { src(init = androidSrc) }
}

val androidSrc get() = initSrc {
    flavors += listOf(androidTest, test)
    main flavour {
        file<AndroidManifest>()
    }
}