package io.bricks.kotlin.dsl.newmodule.module

import io.bricks.kotlin.dsl.newmodule.Dir
import io.bricks.kotlin.dsl.newmodule.Directory

fun Module.src(
    vararg flavors: String,
    init: SourceDirectory.() -> Unit = {}
) = SourceDirectory().also {
    it.flavors += flavors
    it.parent = this
    it.init()
}

class SourceDirectory internal constructor(): Directory() {

    var type: String = "java"

    val flavors = mutableSetOf<String>()

    private var packagePrefix: String? by config

    private val packageName: String? get() = asSequence().filter { it is Mod }.getStringPath(".")

    internal val fullPackageName: String?
        get() = listOfNotNull(
            packageName,
            packagePrefix
        ).takeIf { it.isNotEmpty() }?.reduce(toPath("."))

    init {
        name = "src"
    }

    override fun build() {
        flavors.forEach {
            it.createFlavour()
        }
        super.build()
    }

    infix fun String.flavour(init: Flavour.() -> Unit): Dir = createFlavour()
        .apply { createDefaultPackage() }
        .apply(init)

    private fun String.createFlavour(): Flavour = also { flavors += it }.file()

    private fun Dir.createDefaultPackage(): Dir = apply {
        createSourceTypeDir().createFullPackage()
    }

    private fun Dir.createSourceTypeDir() = type<Dir>()

    private fun Dir.createFullPackage() = withPrefix(fullPackageName?.split("."))

}

class Flavour: Directory() {

    private val src get() = parent as SourceDirectory

    private val type: String get() =  src.type

    private val fullPackageName get() = src.fullPackageName!!

    fun rootPackage(
        init: Dir.() -> Unit = {}
    ): Dir = find<Dir>(type, *fullPackageName
        .split(".")
        .toTypedArray())
        .apply(init)
}

fun initSrc(init: SourceDirectory.() -> Unit) = init

private fun Dir.withPrefix(list: List<String>?): Dir =
    if (list?.isEmpty() != false) this
    else list.first().file<Dir>().withPrefix(list.drop(1))