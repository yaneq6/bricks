package io.bricks.kotlin.dsl.newmodule.source.gradle

import io.bricks.kotlin.dsl.newmodule.source.GradleFile

class JavaGradle: GradleFile() {
    override val template
        get() = """

apply plugin: 'java-library'
apply plugin: 'kotlin'
dependencies {
    implementation dep.kotlin
    testImplementation dep.test
${printDependencies()}
}
sourceCompatibility = "1.8"
targetCompatibility = "1.8"

""".trim()
}