package io.bricks.kotlin.dsl.newmodule

import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

open class Directory: File() {

    open val files: MutableSet<File> = mutableSetOf()

    var root = false

    override fun build() {
        fileName.file.run {
            val created = mkdir()
            if (files.isEmpty()) println("[$created] $path")
        }

        files.forEach { it.build() }
    }

    inline operator fun <reified F: File> String.invoke(
        noinline init: F.() -> Unit = {}
    ): F = file(init)

    inline fun <reified F: File> String.file(
        noinline init: F.() -> Unit = {}
    ): F = file(
        fileType = F::class,
        init = init
    )

    @Suppress("UNCHECKED_CAST")
    fun <F: File> String.file(
        fileType: KClass<F>,
        init: F.() -> Unit = {}
    ): F = files.find {
        it.name == this
    } as? F ?: this@Directory.file(
        fileType = fileType,
        name = this,
        init = init
    )

    inline fun <reified F: File> file(
        noinline init: F.() -> Unit = {}
    ) = file(F::class, init = init)

    fun <F: File> file(
        fileType: KClass<F>,
        name: String? = null,
        init: F.() -> Unit = {}
    ): F = fileType
        .primaryConstructor!!.call()
        .also { it.name = name ?: it.name  }
        .unaryPlus()
        .also(File::selfInit)
        .also(init)

    operator fun <F: File> F.unaryPlus() = apply {
        parent = this@Directory
    }

    override fun match(
        vararg path: String
    ): File? = when (path.size) {
        0 -> null
        1 -> super.match(*path)
        else -> path.drop(1).toTypedArray().let(this::findFile)
    }

    fun findFile(vararg path: String): File? = files.mapNotNull { it.match(*path) }.firstOrNull()

    inline fun <reified T: Spec> find(vararg path: String): T = findFile(*path) as T
}

typealias Dir = Directory