package io.bricks.kotlin.dsl.module.src.main

import io.bricks.kotlin.dsl.base.GenericSpec
import io.bricks.kotlin.dsl.module.ModuleSpec

class LayoutSpec(
    module: ModuleSpec,
    domain: String,
    type: String,
    flavour: String = "main"
) : GenericSpec(
    domain = domain,
    root = module.root,
    dirName = "src",
    flavor = flavour,
    sourceType = "res",
    packageName = "layout",
    fileName = "${type}_$domain.xml".toLowerCase()
) {

    val name = fileName.replace(".xml", "")

    override val template
        get() = """
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    xmlns:tools="http://schemas.android.com/tools">
</android.support.constraint.ConstraintLayout>
""".trim()
}

fun ModuleSpec.layout(
    type: String,
    name: String = domain,
    flavour: String = "main"
) = +LayoutSpec(
    module = this,
    type = type,
    domain = name,
    flavour = flavour
)