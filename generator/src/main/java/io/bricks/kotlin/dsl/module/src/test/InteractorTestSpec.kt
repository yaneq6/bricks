package io.bricks.kotlin.dsl.module.src.test

import io.bricks.kotlin.dsl.base.KSourceSpec
import io.bricks.kotlin.dsl.base.import
import io.bricks.kotlin.dsl.base.test
import io.bricks.kotlin.dsl.module.ModuleSpec
import io.bricks.kotlin.dsl.module.find
import io.bricks.kotlin.dsl.module.src.main.InteractorSpec
import io.bricks.kotlin.dsl.module.src.main.interactor
import io.bricks.kotlin.dsl.module.src.repoMock.RepositoryMockSpec
import io.bricks.kotlin.dsl.module.src.repoMock.repositoryMock

class InteractorTestSpec(
    module: ModuleSpec,
    domain: String,
    val interactor: InteractorSpec,
    val repository: RepositoryMockSpec
) : KSourceSpec(
    module,
    domain,
    type = "InteractorTest",
    flavor = "test"
) {

    override val template
        get() = """
package $packageName

import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import io.bricks.android.base.Repository
import io.bricks.api.interactor.execute
import io.bricks.test.record
${import(interactor)}
${import(interactor.action)}
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.Ignore

class $name {

    private lateinit var repository: Repository
    private lateinit var state: ${interactor.state.name}
    private lateinit var interactor: ${interactor.name}
    private lateinit var actual: List<Any?>

    @Before
    fun setUp() {
        repository = Kodein { import(${repository.moduleName}) }.instance()
        state = ${interactor.state.name}()
        interactor = ${interactor.name}(repository, state)
        actual = record(
            TODO("add state subjects as varargs")
        )
    }

${test()}
    fun execute() {
        // given
        val action = ${interactor.action.name}

        val expected = listOf<Any>(
            TODO("add expected entities of state as varargs")
        )

        // when
        interactor.execute(action)

        // then
        assertEquals(expected, actual)
    }
}

""".trim()

}

fun ModuleSpec.interactorTest(
    name: String = domain,
    repository: RepositoryMockSpec = find(name) { repositoryMock(domain) },
    interactor: InteractorSpec = find(name) { interactor(name) }
) = +InteractorTestSpec(
    module = this,
    domain = name,
    repository = repository,
    interactor = interactor
)