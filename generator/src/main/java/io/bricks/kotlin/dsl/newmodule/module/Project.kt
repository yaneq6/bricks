package io.bricks.kotlin.dsl.newmodule.module

import io.bricks.kotlin.dsl.newmodule.Directory

class Project internal constructor(): Directory() {
    override fun build() {
        fileName.let {
            if (it.isNotEmpty()) it.file
                .also { print(it.absolutePath) }
                .mkdir()
                .also { println(" - $it") }
        }

        println("[root] ${fileName.file.absolutePath}")
        files.forEach { it.build() }
    }
}

fun project(vararg args: Pair<String, Any>, init: Directory.() -> Unit) = Project().apply {
    name = ""
    rebuild = false
    config += args

    init()
}