package io.bricks.kotlin.dsl.newmodule

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class CompoundMapTest {

    lateinit var map: CompoundMap<String, Any?>

    @Before
    fun setup() {
        map = CompoundMap()
        map.parent = CompoundMap()

        map.plusAssign(
            "value" to 1
        )
        map.parent!!.plusAssign(
            "parentValue" to 0
        )
    }

    @Test
    fun getSize() {
        assertEquals(2, map.size)
    }

    @Test
    fun containsKey() {
        assertTrue(map.containsKey("value"))
        assertTrue(map.containsKey("parentValue"))
    }

    @Test
    fun containsValue() {
        assertTrue(map.containsValue(0))
        assertTrue(map.containsValue(1))
    }

    @Test
    fun get() {
        val value by map
        var parentValue by map

        assertEquals(1, value)
        assertEquals(0, parentValue)

        parentValue = 1

        assertEquals(1, parentValue)

        parentValue = null

        assertEquals(0, parentValue)

        assertEquals(1, map["value"])
        assertEquals(0, map["parentValue"])
    }

    @Test
    fun isEmpty() {
        assertFalse(map.isEmpty())
    }

    @Test
    fun getEntries() {
        assertEquals(1, map.entries.size)
    }

    @Test
    fun getKeys() {
        assertEquals(1, map.keys.size)
    }

    @Test
    fun getValues() {
        assertEquals(1, map.values.size)
    }

    @Test
    fun clear() {
    }

    @Test
    fun put() {
    }

    @Test
    fun putAll() {
    }

    @Test
    fun remove() {
    }
}