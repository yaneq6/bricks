package io.bricks.kotlin.dsl.helper

import io.bricks.kotlin.dsl.newmodule.*

class TestSpec(autoTag: Boolean = true, init: TestSpec.() -> Spec) {

    private val map = mutableMapOf<String, Spec>()

    init {
        init().run { if (autoTag) tagRecursive(map) }
    }

    private fun Spec.tagRecursive(map: MutableMap<String, Spec> = mutableMapOf()) {
        map[name] = this
        if (this is Dir) files.forEach { it.tagRecursive(map) }
    }

    fun <S: Spec> S.tag(specName: String? = null) {
        map[specName ?: name] = this
    }

    operator fun get(specName: String) = map[specName]
        ?: throw IllegalArgumentException("Cannot provide Spec for given name: $specName")
}

@Suppress("UNCHECKED_CAST")
infix fun <S: Spec> Spec.test(test: S.() -> Unit) = (this as S).test()

