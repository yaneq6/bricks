package io.bricks.kotlin.dsl.base

import io.bricks.kotlin.dsl.module.ModuleSpec
import org.junit.Assert.assertEquals
import org.junit.Test
import java.io.File

class KSourceSpecTest {

    private val root = "root"
    private val context = "Context"
    private val packageName = "packageName"
    private val type = "Type"
    private val flavour = "flavour"

    private val moduleSpec = ModuleSpec(
        domain = context,
        packageName = packageName,
        root = root
    )

    private val spec = object : KSourceSpec(
        module = moduleSpec,
        domain = context,
        type = type,
        flavor = flavour
    ) {
        override val template get() = "template"
    }

    @Test
    fun getRoot() = assertEquals(root, spec.root)

    @Test
    fun getPackageName() = assertEquals(packageName, spec.packageName)

    @Test
    fun getName() = assertEquals("$context$type", spec.name)

    @Test
    fun getFullName() = assertEquals("$packageName.$context$type", spec.fullName)

    @Test
    fun getFileName() = assertEquals("$context$type.kt", spec.fileName)

    @Test
    fun getContext() = assertEquals(context, spec.domain)

    @Test
    fun getType() = assertEquals(type, spec.type)

    @Test
    fun getFlavor() = assertEquals(flavour, spec.flavor)

    @Test
    fun getOutputDir() = assertEquals(
        File("$root/src/$flavour/java/$packageName").absoluteFile,
        spec.outputDir
    )

    @Test
    fun getOutputFile() = assertEquals(
        File("$root/src/$flavour/java/$packageName/$context$type.kt").absoluteFile,
        spec.outputFile
    )

}