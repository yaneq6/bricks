package io.bricks.kotlin.dsl.module

import io.bricks.kotlin.dsl.module.src.androidTest.activityTest
import io.bricks.kotlin.dsl.module.src.androidTest.androidTestManifest
import io.bricks.kotlin.dsl.module.src.androidTest.testApplication
import io.bricks.kotlin.dsl.module.src.main.*
import io.bricks.kotlin.dsl.module.src.repoImpl.repositoryImpl
import io.bricks.kotlin.dsl.module.src.repoMock.repositoryMock
import io.bricks.kotlin.dsl.module.src.spec.testSpec
import io.bricks.kotlin.dsl.module.src.test.viewModelTest
import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test
import java.io.File

private fun createTestData() = ModuleSpec(
    root = "expected",
    packageName = "expected",
    domain = "Expected"
).run {
    this to listOf(
        "expected/src/main/java/expected/entity/Expected.kt" to entity(),
        "expected/src/main/java/expected/action/Expected.kt" to action(),
        "expected/src/main/java/expected/ExpectedState.kt" to state(),
        "expected/src/repoMock/java/expected/ExpectedRepository.kt" to repositoryMock(),
        "expected/src/repoImpl/java/expected/ExpectedRepository.kt" to repositoryImpl(),
        "expected/src/main/java/expected/interactor/ExpectedInteractor.kt" to interactor(),
        "expected/src/spec/java/expected/ExpectedSpec.kt" to testSpec(),
        "expected/src/main/java/expected/ExpectedViewModel.kt" to viewModel(),
        "expected/src/test/java/expected/ExpectedViewModelTest.kt" to viewModelTest(),
        "expected/src/main/java/expected/ExpectedActivity.kt" to activity(),
        "expected/src/androidTest/java/expected/ExpectedActivityTest.kt" to activityTest(),
        "expected/src/androidTest/java/expected/ExpectedApplication.kt" to testApplication(),
        "expected/src/androidTest/AndroidManifest.xml" to androidTestManifest(),
        "expected/src/main/java/expected/ExpectedModule.kt" to kodeinModule(),
        "expected/build.gradle" to androidGradle()
    )
}

class IntegrationTest {

    private val data = createTestData()
    private val module get() = data.first
    private val specs get() = data.second

    @Ignore
    @Test
    fun buildExpectedData() {
        module.build()
    }

    @Test
    @Ignore // Fixme
    fun testOutputPath() = specs.forEach { (path, spec) ->
        assertEquals(File(path).absoluteFile, spec.outputFile)
    }

    @Test
    @Ignore // Fixme
    fun testTemplate() = specs.forEach { (path, spec) ->
        val expected = path.expected
        val actual = spec.template

        assertEquals(expected, actual)
    }

    private val String.expected get() = let(::File).absoluteFile.readLines().toMultilineString()
}