package io.bricks.kotlin.dsl.newmodule.module

import org.junit.Assert.assertEquals
import org.junit.Test

class JavaKodeinModuleTest {

    @Test
    fun setUp() = project {
        "foo"<Mod> {
            "bar"<Mod> {
                file<JavaKodeinModule> {
                    val expected = listOf(
                        "api dep.kodein",
                        "api project(':foo:bar')"
                    )
                    val actual = buildGradle.dependencies.map(Any::toString)

                    assertEquals(expected, actual)
                }
            }
        }
    }.run { Unit }
}