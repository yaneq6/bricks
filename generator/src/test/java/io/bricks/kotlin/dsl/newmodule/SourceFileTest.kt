package io.bricks.kotlin.dsl.newmodule

import io.bricks.kotlin.dsl.helper.TestSpec
import io.bricks.kotlin.dsl.helper.test
import io.bricks.kotlin.dsl.newmodule.module.Mod
import io.bricks.kotlin.dsl.newmodule.module.project
import io.bricks.kotlin.dsl.newmodule.module.src
import io.bricks.kotlin.dsl.newmodule.source.SourceFile
import org.junit.Assert.assertEquals
import org.junit.Test

class SourceFileTest {

    @Test
    fun getClassName() = testSpec[TestFile].test<SourceFile> {

        val expected = listOf(`package`, prefix, mod, foo, bar, TestFile)
            .reduce { acc, s -> "$acc.$s" }
        val actual = className

        assertEquals(expected, actual)
    }

    @Test
    fun getFullName() = testSpec[TestFile].test<SourceFile> {

        val expected = listOf(mod, "src", main, java, `package`, prefix, mod, foo, bar, TestFile)
            .reduce { acc, s -> "$acc/$s" }
        val actual = fileName

        assertEquals(expected, actual)
    }

    private val testSpec = TestSpec {
        project(
            packagePrefix to "${`package`}.$prefix"
        ) {
            mod<Mod> {
                src {
                    main flavour {
                        rootPackage {
                            foo<Dir> {
                                bar<Dir> {
                                    TestFile<SourceFile> {

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    companion object {
        const val `package` = "package"
        const val prefix = "prefix"
        const val mod = "mod"
        const val main = "main"
        const val java = "java"
        const val foo = "foo"
        const val bar = "bar"
        const val TestFile = "TestFile"
    }
}

