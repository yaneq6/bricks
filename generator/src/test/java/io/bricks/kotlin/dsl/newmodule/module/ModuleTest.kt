package io.bricks.kotlin.dsl.newmodule.module

import org.junit.Assert.assertEquals
import org.junit.Test

class ModuleTest {

    @Test
    fun getModuleName() {
        project {
            "mod1"<Mod> {
                "mod2"<Mod> {
                    "mod3"<Mod> {
                        val expected = ":mod1:mod2:mod3"
                        val actual = moduleName

                        assertEquals(expected, actual)
                    }
                }
            }
        }
    }
}