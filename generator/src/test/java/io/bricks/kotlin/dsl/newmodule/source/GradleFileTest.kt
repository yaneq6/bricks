package io.bricks.kotlin.dsl.newmodule.source

import io.bricks.kotlin.dsl.newmodule.source.TemplateFile.Companion.TAB
import org.junit.Assert.assertEquals
import org.junit.Test

class GradleFileTest {

    private val deps = setOf<Any>(
        "asd1",
        "asd2",
        "asd3"
    )

    @Test
    fun `should print dependencies`() {
        TestGradleFile().run {

            val expected = deps.map { TAB+it }.reduce { acc, s -> "$acc\n$s" }

            dependencies.addAll(deps)

            val actual = template

            assertEquals(expected, actual)
        }
    }

    class TestGradleFile: GradleFile() {
        override val template: String get() = printDependencies()
    }
}