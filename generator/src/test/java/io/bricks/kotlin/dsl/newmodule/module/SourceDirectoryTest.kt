package io.bricks.kotlin.dsl.newmodule.module

import io.bricks.kotlin.dsl.newmodule.Dir
import io.bricks.kotlin.dsl.newmodule.File
import io.bricks.kotlin.dsl.newmodule.packagePrefix
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class SourceDirectoryTest {

    lateinit var src: SourceDirectory


    @Before
    fun setup() {
        project(
            packagePrefix to "foo.bar"
        ) {
            "dir"<Dir> {
                "mod1"<Mod> {
                    "mod2"<Mod> {
                        src = src {

                        }
                    }
                }
            }
        }
    }

    @Test
    fun getFullPackageName() {
        assertEquals("foo.bar.mod1.mod2", src.fullPackageName)
    }

    @Test
    fun flavour() {
        src.apply {
            "flavour" flavour {
                assertEquals("dir/mod1/mod2/src/flavour", toString())
                assertEquals("foo.bar.mod1.mod2", fullPackageName)

                rootPackage {
                    assertEquals("dir/mod1/mod2/src/flavour/java/foo/bar/mod1/mod2", toString())
                    assertEquals("dir/mod1/mod2/src/flavour/java/foo/bar/mod1/mod2/File", "File".file<File>().toString())
                }
            }
        }
    }

    @Test
    fun build() {
    }
}