package io.bricks.kotlin.dsl.newmodule

import org.junit.Before
import org.junit.Test

class MapDelegateTest {

    lateinit var holder: Holder

    @Before
    fun setup() {
        holder = Holder()
    }

    @Test(expected = NoSuchElementException::class)
    fun `should fail`() = with(holder) {
        foo
    }

    @Test
    fun `shouldn't fail`() = with(holder) {
        map["foo"] = Unit

        foo
    }

    @Test
    fun `shouldn't fail 2`() = with(holder) {
        foo = Unit

        foo
    }

    class Holder {
        val map = mutableMapOf<String, Any?>()

        var foo: Unit by map
    }
}