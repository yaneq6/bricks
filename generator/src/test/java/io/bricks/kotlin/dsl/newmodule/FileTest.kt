package io.bricks.kotlin.dsl.newmodule

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.lang.System.*
import java.lang.Thread.*

class FileTest {

    private val file = File()

    @Before
    fun setUp() {
        file.name = "test-" + currentTimeMillis()
        file.parent = null
        file.rebuild = false
    }

    @Test
    fun getName() {
        assertTrue(file.name.startsWith("test"))
    }

    @Test
    fun setParent() {
        val directory1 = Directory()
        val directory2 = Directory()

        file.parent = directory1

        assertTrue(directory1.files.contains(file))
        assertFalse(directory2.files.contains(file))

        file.parent = directory2

        assertFalse(directory1.files.contains(file))
        assertTrue(directory2.files.contains(file))
    }

    @Test
    fun getFullName() {
        file.parent = Directory().apply {
            name = "parent"
        }

        assertTrue(file.fileName.startsWith("parent/test"))
    }

    @Test
    fun build(): Unit = java.io.File(file.fileName).run {
        file.build()
        assertTrue(exists())
        delete()
    }

    @Test
    fun `should rebuild`(): Unit = java.io.File(file.fileName).run {
        file.rebuild = true

        file.build()
        val timestamp1 = lastModified()

        sleep(1000)

        file.build()
        val timestamp2 = lastModified()

        assertNotEquals(timestamp1, timestamp2)
        delete()
    }

    @Test
    fun `should't rebuild`(): Unit = java.io.File(file.fileName).run {
        file.rebuild = false

        file.build()
        val timestamp1 = lastModified()

        sleep(1000)

        file.build()
        val timestamp2 = lastModified()

        assertEquals(timestamp1, timestamp2)
        delete()
    }
}

