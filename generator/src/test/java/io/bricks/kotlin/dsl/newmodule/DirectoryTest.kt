package io.bricks.kotlin.dsl.newmodule

import io.bricks.kotlin.dsl.newmodule.module.project
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class DirectoryTest {

    lateinit var main: Dir
    lateinit var foo: Dir
    lateinit var bar: Dir
    lateinit var file: Dir

    @Before
    fun setUp() {
        project {
            main = "main" {
                foo = "foo" {
                    bar = "bar" {
                        file = "file" {

                        }
                    }
                }
            }
        }

    }

    @Test
    fun findFile() {
        val actual = main.findFile("foo", "bar", "file")

        assertEquals(file.name, actual?.name)
    }
}