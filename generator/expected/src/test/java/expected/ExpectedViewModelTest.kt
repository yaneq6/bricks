package expected

import org.kodein.di.Kodein
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.Ignore

class ExpectedViewModelTest : ExpectedSpec {

    lateinit var viewModel: ExpectedViewModel

    @Before
    fun setup() {
        val kodein = Kodein {
            import(expectedRepositoryModule)
            import(expectedModule)
        }
        viewModel = ExpectedViewModel(kodein)
    }

    @Test
    fun test() {
        TODO()
    }

}