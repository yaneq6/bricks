package expected

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import io.bricks.android.kodein.bindViewModel
import io.bricks.api.interactor.bindInteractor
import io.bricks.api.interactor.bindInteractors
import expected.interactor.ExpectedInteractor

val expectedModule = Kodein.Module("expectedModule") {
    bindInteractors()
    bind() from singleton { ExpectedState() }
    bindInteractor { ExpectedInteractor(instance(), instance()) }
    bindViewModel<ExpectedViewModel>()
}