package expected

import android.arch.lifecycle.ViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import io.bricks.android.base.Controller

class ExpectedViewModel(
    override val kodein: Kodein
) : ViewModel(),
    KodeinAware,
    Controller by kodein.instance() {

    private val state = instance<ExpectedState>()

}