package expected

import android.os.Bundle
import org.kodein.di.Kodein
import org.kodein.di.generic.kodein
import io.bricks.android.base.BaseActivity
import io.bricks.android.base.controllerModule
import io.bricks.android.base.schedulersModule
import io.bricks.android.kodein.KodeinViewModelFactory
import io.bricks.android.kodein.provideViewModel

class ExpectedActivity : BaseActivity() {

    private val viewModel by lazy {
        provideViewModel<ExpectedViewModel>(KodeinViewModelFactory(kodein().value))
    }
//    private val viewModel: ExpectedViewModel by instance()

    override fun provideOverridingModule() = Kodein.Module("ExpectedActivity") {
//        import(repositoryModule)
        import(schedulersModule)
        import(controllerModule)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expected)
    }

}