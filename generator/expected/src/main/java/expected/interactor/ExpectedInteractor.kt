package expected.interactor

import io.bricks.android.base.Repository
import io.bricks.api.interactor.UseCase
import expected.action.Expected

import expected.ExpectedState

class ExpectedInteractor(
    private val repository: Repository,
    private val state: ExpectedState
) : UseCase<Expected, Unit> {

    override fun Expected.execute(): Unit = TODO("not implemented")
}