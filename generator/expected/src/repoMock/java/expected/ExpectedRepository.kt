package expected

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import io.bricks.android.base.Repository
import io.bricks.api.action.Action
import expected.action.Expected

class ExpectedRepositoryMock : Repository {

    @Suppress("IMPLICIT_CAST_TO_ANY", "UNCHECKED_CAST")
    override fun <A : Action<R>, R> dispatch(action: A): R = when (action) {
        is Expected -> TODO()
        else -> throw Exception("Unhandled action exception: $action")
    } as R
}

val expectedRepositoryModule = Kodein.Module("expectedRepositoryModule") {
    bind() from singleton<Repository> { ExpectedRepositoryMock() }
}