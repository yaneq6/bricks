package expected

import android.app.Application
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware

class ExpectedApplication : Application(), KodeinAware {

    override val kodein = Kodein {}

}