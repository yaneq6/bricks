package expected

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import org.kodein.di.generic.kodein
import io.bricks.android.base.BaseActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.Ignore
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ExpectedActivityTest : ExpectedSpec,
                          KodeinAware {

    @Rule
    @JvmField
    val rule = ActivityTestRule(ExpectedActivity::class.java)

    override val kodein: Kodein get() = (rule.activity as BaseActivity).kodein().value

    @Before
    fun setUp() {

    }

    @Test
    fun test() {

    }
}