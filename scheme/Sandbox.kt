import io.bricks.kotlin.dsl.newmodule.android
import io.bricks.kotlin.dsl.newmodule.module.AndroidModule
import io.bricks.kotlin.dsl.newmodule.module.JavaModule
import io.bricks.kotlin.dsl.newmodule.module.Mod
import io.bricks.kotlin.dsl.newmodule.module.project
import io.bricks.kotlin.dsl.newmodule.packagePrefix
import io.bricks.kotlin.dsl.newmodule.source.gradle.SettingsGradle

class Sandbox: BaseScheme() {

    val sandbox = "sandbox"

    override val spec = project(
        packagePrefix to "io.bricks"
    ) {
        file<SettingsGradle> {
            rebuild = true
        }
        sandbox<Mod> {
            android<Mod> {
                "auth"<AndroidModule>()
                "base"<JavaModule>()
                "test"<JavaModule>()
                "androidTest"<AndroidModule>()
                "core"<AndroidModule>()
                "feature"<AndroidModule>()
            }
            "echo"<Mod> {
                "android"<AndroidModule>()
                "api"<JavaModule>()
                "ktor"<JavaModule>()
            }
        }
    }
}