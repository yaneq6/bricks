import io.bricks.kotlin.dsl.newmodule.*
import io.bricks.kotlin.dsl.newmodule.module.*
import io.bricks.kotlin.dsl.newmodule.source.gradle.SettingsGradle

class Framework: BaseScheme() {

    private val interaction = "interaction"
    private val property = "property"
    private val rx = "rx"
    private val databinding = "databinding"

    override val spec = project(
        packagePrefix to "io.bricks"
    ) {
        file<SettingsGradle> {
            rebuild = true
        }
        framework<Dir> {
            android<Mod> {
                ui<AndroidModule>()
                core<AndroidModule>()
            }
            model<Mod> {
                standard<JavaModule> {
                    file<JavaKodeinModule>()
                }
            }
            interaction<Mod> {
                standard<JavaModule> {
                    file<JavaKodeinModule>()
                }
            }
            property<Mod> {
                core<JavaModule>()
            }
            rx<Mod> {
                databinding<AndroidModule>()
            }
        }
    }
}

