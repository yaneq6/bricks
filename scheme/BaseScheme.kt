import io.bricks.kotlin.dsl.newmodule.Spec
import org.junit.Ignore
import org.junit.Test

abstract class BaseScheme(
    private val ignore: Boolean = false
) {

    abstract val spec: Spec

    @Test
    fun build() = if (!ignore) spec.build() else print("[IGNORED] scheme ${this.javaClass.name}")
}