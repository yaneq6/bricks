import io.bricks.kotlin.dsl.newmodule.module.AndroidModule
import io.bricks.kotlin.dsl.newmodule.module.Mod
import io.bricks.kotlin.dsl.newmodule.module.project
import io.bricks.kotlin.dsl.newmodule.packagePrefix
import io.bricks.kotlin.dsl.newmodule.source.gradle.SettingsGradle

class Uber : BaseScheme(ignore = true) {

    override val spec = project(
        packagePrefix to "io.bricks"
    ) {
        file<SettingsGradle>()
        "uber"<Mod> {
            "api"<AndroidModule> {
                "fuel"<AndroidModule>{
                }
            }
            "invoices"<AndroidModule>()
            "app"<AndroidModule>()
        }
    }
}