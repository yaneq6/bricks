package io.bricks.sandbox.android.echo

abstract class EchoSpec {

    private companion object {
        const val MESSAGE = "message"
        const val EMPTY_MESSAGE = ""
        val MESSAGES = listOf(MESSAGE)
    }

    abstract fun writeMessage(message: String)
    abstract fun clickSendButton()

    abstract fun assertButtonEnabled(enabled: Boolean)
    abstract fun assertMessages(messages: List<String>)
    abstract fun assertMessageInput(message: String)


    fun expectMessagesIfWriteMessageAndClickButton() {
        writeMessage(MESSAGE)
        clickSendButton()

        assertMessages(MESSAGES)
    }

    fun expectEmptyMessageIfWriteMessageAndClickButton() {
        writeMessage(MESSAGE)
        clickSendButton()

        assertMessageInput(EMPTY_MESSAGE)
    }

    fun expectDisabledButtonIfWriteEmptyMessage() {
        writeMessage(EMPTY_MESSAGE)

        assertButtonEnabled(false)
    }

    fun expectButtonEnabledIfWriteMessage() {
        writeMessage(MESSAGE)

        assertButtonEnabled(true)
    }
}