package io.bricks.sandbox.android.echo

import io.bricks.api.UseCase
import io.bricks.network.core.ApiAdapter
import io.bricks.sandbox.api.echo.Message
import io.bricks.sandbox.api.echo.SendEcho
import io.reactivex.subjects.BehaviorSubject

fun <T1, T2> BehaviorSubject<T1>.append(new: T2, append: T1.(T2) -> T1) {
    onNext(value.append(new))
}

class SendEchoInteractor(
    private val api: ApiAdapter,
    private val state: EchoState
): UseCase<SendEcho, Message> {

    override fun SendEcho.execute() = let(api::call).also(updateState)

    private val updateState: (Message) -> Unit = {
        state.messages.append(it, Messages::plus)
    }
}