package io.bricks.sandbox.android.echo

//import io.bricks.sandbox.android.echo.di.AppModule
//import io.bricks.sandbox.android.echo.di.appModule
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges
import io.bricks.android.ui.RecyclerViewAdapter
import io.bricks.android.ui.ViewHolder
import io.bricks.sandbox.android.echo.di.echoModelModule
import io.bricks.sandbox.api.echo.Message
import kotlinx.android.synthetic.main.activity_echo.*
import kotlinx.android.synthetic.main.item_echo.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.android.retainedKodein
import org.kodein.di.generic.instance

class EchoActivity: AppCompatActivity(), KodeinAware {

    private val parentKodein by closestKodein()

    override val kodein by retainedKodein {
        extend(parentKodein)
//        import(appModule)
        import(echoModelModule)
    }

    private val viewModel by instance<EchoViewModel>()
//    private val viewModel by lazy { EchoModule(AppModule(this)).echoViewModel }

    private val echoAdapter = RecyclerViewAdapter<Message, ViewHolder>(
        layoutResId = R.layout.item_echo,
        createViewHolder = ::ViewHolder,
        bindData = { itemView.todoTextView.text = it.text }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(0)
        setContentView(R.layout.activity_echo)

        todoRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@EchoActivity)
            adapter = echoAdapter
        }

        with(viewModel) {
            +messageInput.textChanges()
                .map(CharSequence::toString)
                .subscribe(message::onNext)

            +sendMessageButton.clicks()
                .observeOn(schedulers.backgroundScheduler)
                .subscribe { sendMessage() }

            +clearMessageInput
                .observeOn(schedulers.mainScheduler)
                .subscribe { messageInput.editableText.clear() }

            +enableSendButton.subscribe {
                sendMessageButton.isEnabled = it
            }

            +messages.map(Messages::list).observeOn(schedulers.mainScheduler).subscribe {
                echoAdapter.items = it
                todoRecyclerView.smoothScrollToPosition(it.size)
            }
        }
    }

    override fun onDestroy() {
        viewModel.unbind()
        super.onDestroy()
    }
}

