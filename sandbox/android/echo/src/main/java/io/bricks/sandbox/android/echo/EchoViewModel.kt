package io.bricks.sandbox.android.echo

import io.bricks.api.ActionStatus
import io.bricks.api.UseCase
import io.bricks.interaction.standard.handler.filter
import io.bricks.model.standard.Model
import io.bricks.sandbox.api.echo.Message
import io.bricks.sandbox.api.echo.SendEcho
import io.reactivex.subjects.BehaviorSubject

class EchoViewModel(
    model: Model,
    private val state: EchoState,
    private val sendEchoUseCase: UseCase<SendEcho, Message>
): Model by model {

    val message = BehaviorSubject.create<String>()!!

    val clearMessageInput get() = status.filter<ActionStatus.Start, SendEcho>().map { Unit }!!

    val messages get() = state.messages

    val enableSendButton get() = message.map(String::isNotBlank)!!

    fun sendMessage() {
        message.value?.let {
            sendEchoUseCase(SendEcho(Message(it)))
        }
    }
}