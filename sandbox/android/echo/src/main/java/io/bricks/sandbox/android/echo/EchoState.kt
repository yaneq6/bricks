package io.bricks.sandbox.android.echo

import io.bricks.cache.core.createCache
import io.bricks.cache.rx.cached
import io.bricks.state.core.CachedState
import io.reactivex.subjects.BehaviorSubject

class EchoState(
    cachedState: CachedState
) :
    CachedState by cachedState {

    val messages = BehaviorSubject
        .createDefault(Messages())
        .cached(createCache("messages"))
}