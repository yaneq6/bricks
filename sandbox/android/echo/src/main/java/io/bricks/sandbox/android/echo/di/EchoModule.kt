package io.bricks.sandbox.android.echo.di

import io.bricks.api.UseCase
import io.bricks.model.standard.Model
import io.bricks.network.core.ApiAdapter
import io.bricks.sandbox.android.echo.EchoState
import io.bricks.sandbox.android.echo.EchoViewModel
import io.bricks.sandbox.android.echo.SendEchoInteractor
import io.bricks.sandbox.api.echo.Message
import io.bricks.sandbox.api.echo.SendEcho
import io.bricks.state.core.CachedState

class EchoModule(
    private val apiAdapter: ApiAdapter,
    cachedState: CachedState,
    model: Model
) {
//    constructor(appModule: AppModule) : this(
//        apiAdapter = appModule.apiAdapter,
//        cachedState = appModule.cachedState,
//        model = appModule.model
//    )

    private val echoState = EchoState(
        cachedState = cachedState
    )

    private val sendEchoUseCase: UseCase<SendEcho, Message>
        get() = SendEchoInteractor(
            api = apiAdapter,
            state = echoState
        )

    val echoViewModel = EchoViewModel(
        model = model,
        sendEchoUseCase = sendEchoUseCase,
        state = echoState
    )
}