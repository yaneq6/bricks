package io.bricks.sandbox.android.echo.di

import io.bricks.api.UseCase
import io.bricks.sandbox.android.echo.EchoState
import io.bricks.sandbox.android.echo.EchoViewModel
import io.bricks.sandbox.android.echo.SendEchoInteractor
import io.bricks.sandbox.api.echo.Message
import io.bricks.sandbox.api.echo.SendEcho
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

val echoModelModule = Kodein.Module("echoModelModule") {
    bind() from singleton {
        EchoState(
            cachedState = instance()
        )
    }
    bind<UseCase<SendEcho, Message>>() with provider {
        SendEchoInteractor(
            state = instance(),
            api = instance()
        )
    }
    bind() from singleton {
        EchoViewModel(
            model = instance(),
            state = instance(),
            sendEchoUseCase = instance()
        )
    }
}