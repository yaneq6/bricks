package io.bricks.sandbox.android.echo

import io.bricks.sandbox.api.echo.Message

data class Messages(
    val list: List<Message> = emptyList()
) {

    operator fun plus(message: Message) = Messages(list + message)
}