package io.bricks.sandbox.android.echo

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import com.agoda.kakao.*
import org.hamcrest.Matcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware

@RunWith(AndroidJUnit4::class)
class EchoActivityTest: KodeinAware {

    override val kodein: Kodein get() = (rule.activity as EchoActivity).kodein

    @Rule
    @JvmField
    val rule = ActivityTestRule(EchoActivity::class.java)

    private val formScreen = EchoScreen()

    @Test
    fun shouldSendMessages() {
        formScreen {
            (0..10).forEach {
                messageInput.replaceText("test$it")
                button.click()
            }
            list {
                lastChild<MessageItem> {
                    isVisible()
                    todoTextView { hasText("test10") }
                }
            }
        }
    }
}

class EchoScreen: Screen<EchoScreen>() {
    val messageInput = KEditText { withId(R.id.messageInput) }
    val button = KButton { withId(R.id.sendMessageButton) }
    val list = KRecyclerView(
        builder = { withId(R.id.todoRecyclerView) },
        itemTypeBuilder = { itemType(::MessageItem) }
    )
}

class MessageItem(matcher: Matcher<View>): KRecyclerItem<MessageItem>(matcher) {
    val todoTextView = KTextView(matcher) { withId(R.id.todoTextView) }
}