package io.bricks.sandbox.android.echo

import io.bricks.api.ActionStatus
import io.bricks.cache.core.CachedProperty
import io.bricks.network.core.ApiAdapter
import io.bricks.network.core.ApiMethod
import io.bricks.sandbox.android.echo.di.echoModelModule
import io.bricks.sandbox.android.test.testModule
import io.bricks.sandbox.api.echo.Message
import io.bricks.sandbox.api.echo.SendEcho
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.kodein.di.Kodein
import org.kodein.di.direct
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance

class EchoViewModelOldTest {

    private lateinit var model: EchoViewModel
    private val cachedPropertyFactory = mockk<CachedProperty.Factory>()

    init {
        every { cachedPropertyFactory.create<Messages>(any(), any()) } returns TestCachedProperty()
    }

    @Before
    fun setup() {
        model = Kodein {
            import(testModule)
            import(echoModelModule)
            import(testApiAdapterModule)
            bind<CachedProperty.Factory>() with instance(cachedPropertyFactory)

        }.direct.instance()

    }

    @Test
    fun shouldObserveMessageText(): Unit = with(model) {
        /*given*/
        val values = listOf(
            "a",
            "b",
            "c"
        )
        val messageText = message.test()

        /*when*/
        values.forEach(message::onNext)

        /*then*/
        messageText.assertValues(*values.toTypedArray())
    }

    @Test
    fun shouldDisableSendButtonWhenTextIsEmpty(): Unit = with(model) {
        /*given*/
        val expectedButtonStatus = enableSendButton.test()

        /*when*/
        message.apply {
            onNext("text")
            onNext("")
        }

        /*then*/
        expectedButtonStatus.assertValues(true, false)
    }

    @Test
    fun shouldSendMessage(): Unit = with(model) {
        /*given*/
        val errors = status.filter { it is ActionStatus.Error }.test()

        val messageList = listOf("message1", "message2", "message3")

        val expected = messageList.map(::Message).let(::Messages)

        /*when*/
        messageList.forEach {
            message.onNext(it)
            sendMessage()
        }

        val actual = messages.value

        /*then*/
        errors.assertNoValues()

        assertEquals(expected, actual)

    }
}

private val testApiAdapterModule = Kodein.Module("testApiAdapterModule") {
    bind<ApiAdapter>() with instance(api)
}

private val api = object : ApiAdapter {

    override val config: ApiAdapter.Config = ApiAdapter.Config()

    @Suppress("UNCHECKED_CAST")
    override fun <M: ApiMethod<T>, T: Any> call(method: M): T = when (method) {
        is SendEcho -> method.message
        else -> TODO()
    } as T
}