package io.bricks.sandbox.android.echo

import io.bricks.cache.core.CachedProperty
import io.bricks.network.core.ApiAdapter
import io.bricks.network.core.ApiMethod
import io.bricks.sandbox.android.echo.di.echoModelModule
import io.bricks.sandbox.android.test.testModule
import io.bricks.sandbox.api.echo.Message
import io.bricks.sandbox.api.echo.SendEcho
import io.mockk.every
import io.mockk.mockk
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.kodein.di.Kodein
import org.kodein.di.direct
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance

class EchoViewModelIntegrationTest : EchoSpec() {

    private val cachedPropertyFactory = mockk<CachedProperty.Factory>()

    init {
        every { cachedPropertyFactory.create<Messages>(any(), any()) } returns TestCachedProperty()
    }

    private lateinit var model: EchoViewModel
    private lateinit var buttonObserver: TestObserver<Boolean>
    private lateinit var messagesObserver: TestObserver<List<String>>
    private lateinit var messageInputObserver: TestObserver<String>

    @Before
    fun setup() {
        model = Kodein {
            import(testModule)
            import(testApiAdapterModule)
            import(echoModelModule)
            bind<CachedProperty.Factory>() with instance(cachedPropertyFactory)
        }.direct.instance()

        buttonObserver = model.enableSendButton.test()
        messagesObserver = model.messages.map { it.list.map(Message::text) }.test()
        messageInputObserver = model.message.test()
        model.clearMessageInput.subscribe { model.message.onNext("") }
    }

    override fun writeMessage(message: String) {
        model.message.onNext(message)
    }

    override fun clickSendButton() {
        model.sendMessage()
    }

    override fun assertButtonEnabled(enabled: Boolean) {
        buttonObserver.assertValue(enabled)
    }

    override fun assertMessages(messages: List<String>) {
        assertEquals(messages, messagesObserver.values().last())
    }

    override fun assertMessageInput(message: String) {
        assertEquals(message, messageInputObserver.values().last())
    }

    @Test
    fun test1() = expectEmptyMessageIfWriteMessageAndClickButton()

    @Test
    fun test2() = expectDisabledButtonIfWriteEmptyMessage()

    @Test
    fun test3() = expectMessagesIfWriteMessageAndClickButton()
}

private val testApiAdapterModule = Kodein.Module("testApiAdapterModule") {
    bind<ApiAdapter>() with instance(api)
}

private val api = object : ApiAdapter {

    override val config: ApiAdapter.Config = ApiAdapter.Config()

    @Suppress("UNCHECKED_CAST")
    override fun <M: ApiMethod<T>, T: Any> call(method: M): T = when (method) {
        is SendEcho -> method.message
        else -> TODO()
    } as T
}

internal class TestCachedProperty : CachedProperty<Messages> {
    override var value: Messages? = null
}