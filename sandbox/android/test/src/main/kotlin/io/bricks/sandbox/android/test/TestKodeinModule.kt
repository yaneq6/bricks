package io.bricks.sandbox.android.test

import io.bricks.interaction.standard.kodein.actionHandlerModule
import io.bricks.model.standard.kodein.modelModule
import io.bricks.rx.core.comonent.dummyRxSchedulersModule
import io.bricks.rx.core.comonent.rxComponentModule
import io.bricks.serial.jsonFactoryModule
import io.bricks.state.core.cachedStateModule
import org.kodein.di.Kodein

val testModule = Kodein.Module("testModule") {
    import(jsonFactoryModule)
    import(dummyRxSchedulersModule)
    import(cachedStateModule)
    import(rxComponentModule)
    import(actionHandlerModule)
    import(modelModule)
}