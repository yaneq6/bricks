package io.bricks.sandbox.android.core

import android.app.Application
import android.content.Context
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance

class SandboxApplication : Application(), KodeinAware {
    override val kodein = Kodein {
        bind<Context>() with instance(this@SandboxApplication)
        import(coreModule)
    }
}