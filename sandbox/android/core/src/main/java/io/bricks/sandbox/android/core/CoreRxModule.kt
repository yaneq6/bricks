package io.bricks.sandbox.android.core

import android.content.Context
import io.bricks.cache.core.CachedProperty
import io.bricks.cache.sharedpreferences.SharedPreferencesCachedProperty.*
import io.bricks.interaction.standard.handler.ActionHandler
import io.bricks.interaction.standard.handler.ActionHandlerImpl
import io.bricks.model.standard.Model
import io.bricks.model.standard.ModelImpl
import io.bricks.network.core.ApiAdapter
import io.bricks.network.fuel.FuelApiAdapter
import io.bricks.rx.core.CompositeRxComponent
import io.bricks.rx.core.RxComponent
import io.bricks.rx.core.RxSchedulers
import io.bricks.serial.core.StringJsonFactory
import io.bricks.serial.jackson.JacksonFactory
import io.bricks.state.core.CachedState
import io.bricks.state.core.CachedStateImpl
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.Singles
import io.reactivex.rxkotlin.toSingle
import io.reactivex.schedulers.Schedulers
import kotlin.reflect.KClass

open class CoreRxModule(
    context: Single<Context>
) {

    private val jsonFactory = JacksonFactory()
        .toSingle<StringJsonFactory>()

    private val config = ApiAdapter.Config(
        host = "10.0.2.2:8080"
    ).toSingle()

    private val log = { any: Any -> println(any) }.toSingle()

    private val apiAdapter = Singles.zip(
        config,
        jsonFactory,
        log,
        ::FuelApiAdapter
    ).map { it as ApiAdapter }.cache()!!


    private val createSharedPreferences = context.map {
        { type: KClass<*> ->
            it.getSharedPreferences(
                type.java.name,
                Context.MODE_PRIVATE
            )
        }
    }.cache()

    private val cacheFactory by lazy {
        Singles.zip(
            createSharedPreferences,
            jsonFactory,
            ::Factory
        ).map { it as CachedProperty.Factory }
    }

    open val rxSchedulers by lazy {
        RxSchedulers(
            mainScheduler = AndroidSchedulers.mainThread(),
            backgroundScheduler = Schedulers.io()
        ).toSingle()
    }

    val cachedState by lazy {
        cacheFactory.map<CachedState>(::CachedStateImpl).cache()
    }


    private val actionHandler by lazy {
        ActionHandlerImpl()
            .toSingle<ActionHandler>()
    }

    private val rxComponent by lazy {
        rxSchedulers.map<RxComponent>(::CompositeRxComponent)
    }

    val model by lazy {
        Singles.zip(
            actionHandler,
            rxComponent,
            ::ModelImpl
        ).map { it as Model }
    }

}