package io.bricks.sandbox.android.core

import android.content.Context
import android.content.SharedPreferences
import io.bricks.cache.core.CachedProperty
import io.bricks.cache.sharedpreferences.SharedPreferencesCachedProperty.*
import io.bricks.interaction.standard.handler.ActionHandler
import io.bricks.interaction.standard.handler.ActionHandlerImpl
import io.bricks.model.standard.Model
import io.bricks.model.standard.ModelImpl
import io.bricks.network.core.ApiAdapter
import io.bricks.network.fuel.FuelApiAdapter
import io.bricks.rx.core.CompositeRxComponent
import io.bricks.rx.core.RxComponent
import io.bricks.rx.core.RxSchedulers
import io.bricks.serial.core.JsonFactory
import io.bricks.serial.jackson.JacksonFactory
import io.bricks.state.core.CachedState
import io.bricks.state.core.CachedStateImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlin.reflect.KClass

open class CoreModule(
    val context: Context
) {

    val jsonFactory: JsonFactory<String> by lazy {
        JacksonFactory()
    }

    val config = ApiAdapter.Config(
        host = "10.0.2.2:8080"
    )

    val apiAdapter: ApiAdapter by lazy {
        FuelApiAdapter(
            config = config,
            log = ::println,
            jsonFactory = jsonFactory
        )
    }

    val createSharedPreferences: (KClass<*>) -> SharedPreferences = {
        context.getSharedPreferences(it.java.name, Context.MODE_PRIVATE)
    }

    val cacheFactory: CachedProperty.Factory by lazy {
        Factory(
            sharedPreferences = createSharedPreferences,
            jsonFactory = jsonFactory
        )
    }

    val cachedState: CachedState by lazy {
        CachedStateImpl(
            cacheFactory = cacheFactory
        )
    }

    open val rxSchedulers by lazy {
        RxSchedulers(
            mainScheduler = AndroidSchedulers.mainThread(),
            backgroundScheduler = Schedulers.io()
        )
    }

    val rxComponent: RxComponent
        get() = CompositeRxComponent(
            schedulers = rxSchedulers
        )

    val actionHandler: ActionHandler get() = ActionHandlerImpl()

    val model: Model
        get() = ModelImpl(
            rxComponent = rxComponent,
            handler = actionHandler
        )

}