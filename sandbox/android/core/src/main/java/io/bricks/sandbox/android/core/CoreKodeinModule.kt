package io.bricks.sandbox.android.core

import io.bricks.android.core.kodein.schedulersModule
import io.bricks.cache.sharedpreferences.cachedPropertyModule
import io.bricks.interaction.standard.kodein.actionHandlerModule
import io.bricks.model.standard.kodein.modelModule
import io.bricks.network.apiAdapterModule
import io.bricks.network.core.ApiAdapter
import io.bricks.rx.core.comonent.rxComponentModule
import io.bricks.serial.jsonFactoryModule
import io.bricks.state.core.cachedStateModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance

val coreModule = Kodein.Module("coreKodein") {
    bind() from instance(ApiAdapter.Config(host = "10.0.2.2:8080"))
    import(jsonFactoryModule)
    import(apiAdapterModule)
    import(schedulersModule)
    import(cachedPropertyModule)
    import(cachedStateModule)
    import(rxComponentModule)
    import(actionHandlerModule)
    import(modelModule)
}