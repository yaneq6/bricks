package io.bricks.sandbox.android.core

import android.content.Context
import android.content.SharedPreferences
import io.bricks.rx.core.RxSchedulers
import io.mockk.every
import io.mockk.mockk
import io.reactivex.schedulers.Schedulers
import org.junit.Test

class CoreModuleTest : Benchmark {

    private val context = mockk<Context>()
    private val sharedPreferences = mockk<SharedPreferences>(relaxed = true)
    private val editor = mockk<SharedPreferences.Editor>(relaxed = true)

    init {
        every { context.getSharedPreferences(any(), any()) } returns sharedPreferences
        every { sharedPreferences.getString(any(), any()) } returns null
        every { sharedPreferences.edit() } returns editor
    }

    @Test
    fun test1() = testCase("custom1") {
        TestCoreModule(context).run {
            model
            cachedState
        }
    }

    @Test
    fun test2() = testCase("custom1") {
        TestCoreModule(context).run {
            model
            cachedState
        }
    }


    class TestCoreModule(context: Context): CoreModule(context) {
        override val rxSchedulers = RxSchedulers(
            mainScheduler = Schedulers.io(),
            backgroundScheduler = Schedulers.io()
        )
    }
}