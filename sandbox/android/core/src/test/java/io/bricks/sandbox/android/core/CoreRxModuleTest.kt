package io.bricks.sandbox.android.core

import android.content.Context
import android.content.SharedPreferences
import io.bricks.rx.core.RxSchedulers
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import io.reactivex.rxkotlin.toSingle
import io.reactivex.schedulers.Schedulers
import org.junit.Test
import kotlin.system.measureNanoTime

class CoreRxModuleTest : Benchmark {

    val context = mockk<Context>()
    val singleContext = context.toSingle()
    val sharedPreferences = mockk<SharedPreferences>(relaxed = true)
    val editor = mockk<SharedPreferences.Editor>(relaxed = true)

    init {
        every { context.getSharedPreferences(any(), any()) } returns sharedPreferences
        every { sharedPreferences.getString(any(), any()) } returns null
        every { sharedPreferences.edit() } returns editor
    }

    @Test
    fun test1() = testCase("rx1") {
        TestCoreRxModule(singleContext).run {
            model.blockingGet()
            cachedState.blockingGet()
        }
    }

    @Test
    fun test2() = testCase("rx2") {
        TestCoreRxModule(singleContext).run {
            model.blockingGet()
            cachedState.blockingGet()
        }
    }

    class TestCoreRxModule(context: Single<Context>): CoreRxModule(context) {
        override val rxSchedulers: Single<RxSchedulers> = RxSchedulers(
            Schedulers.io(),
            Schedulers.io()
        ).toSingle()
    }
}