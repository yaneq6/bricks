package io.bricks.sandbox.android.core

import android.content.Context
import android.content.SharedPreferences
import io.bricks.model.standard.Model
import io.bricks.rx.core.RxSchedulers
import io.bricks.state.core.CachedState
import io.mockk.every
import io.mockk.mockk
import org.junit.Test
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import kotlin.system.measureNanoTime

class CoreKodeinModuleTest : Benchmark {

    private val context = mockk<Context>()
    private val sharedPreferences = mockk<SharedPreferences>(relaxed = true)
    private val editor = mockk<SharedPreferences.Editor>(relaxed = true)

    init {
        every { context.getSharedPreferences(any(), any()) } returns sharedPreferences
        every { sharedPreferences.getString(any(), any()) } returns null
        every { sharedPreferences.edit() } returns editor
    }

    @Test
    fun test1() = testCase("kodein1") {
        Kodein {
            import(coreModule)
            bind<Context>() with instance(context)
            bind<RxSchedulers>(overrides = true) with instance(RxSchedulers.dummy())
        }.run {
            instance<Model>()
            instance<CachedState>()
        }
    }

    @Test
    fun test2() = testCase("kodein2") {
        Kodein {
            import(coreModule)
            bind<Context>() with instance(context)
            bind<RxSchedulers>(overrides = true) with instance(RxSchedulers.dummy())
        }.run {
            instance<Model>()
            instance<CachedState>()
        }
    }
}