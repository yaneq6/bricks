package io.bricks.sandbox.android.core

import kotlin.system.measureNanoTime

interface Benchmark {

    fun testCase(name: String, test: () -> Unit) {
        info(name, measureNanoTime { test() })
    }

    private fun info(funName: String, timeNano: Long) {
        val className = this::class.java.simpleName
        println("$className.$funName: $timeNano")
    }
}

