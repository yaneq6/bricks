import org.kodein.di.Kodein
import io.bricks.api.bindInteractor
import io.bricks.api.bindInteractors

val interactorsModule = Kodein.Module("interactorsModule") {
    bindInteractors()
    bindInteractor { SendEchoInteractor() }
}