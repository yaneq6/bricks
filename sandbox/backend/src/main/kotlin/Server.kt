import io.bricks.interaction.ktor.handler.ActionHandler
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.Compression
import io.ktor.features.DefaultHeaders
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.server.netty.NettyApplicationEngine
import org.kodein.di.DKodein
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val serverModule = Kodein.Module("serverModule") {
    bind() from singleton { server }
    bind("start") from singleton { instance<NettyApplicationEngine>().start(wait = true) }
}

val DKodein.server: NettyApplicationEngine
    get() = embeddedServer(
        factory = Netty,
        port = 8080
    ) {
        install(DefaultHeaders)
        install(Compression)
        install(CallLogging)
        install(ActionHandler)
    }

private inline val DKodein.ActionHandler get() = instance<ActionHandler.Feature>()















class Server(
    private val actionHandler: ActionHandler.Feature
) {

    private val server = embeddedServer(
        factory = Netty,
        port = 8080
    ) {
        install(DefaultHeaders)
        install(Compression)
        install(CallLogging)
        install(actionHandler)
    }

    fun start() {
        server.start(wait = true)
    }
}
