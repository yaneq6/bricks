import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import io.bricks.interaction.ktor.handler.actionHandlerModule
import io.bricks.serial.jsonFactoryModule
import io.ktor.server.netty.NettyApplicationEngine
import org.kodein.di.direct

fun main(args: Array<String>): Unit = Kodein {
    import(jsonFactoryModule)
    import(actionHandlerModule)
    import(interactorsModule)
    import(serverModule)
}.direct.run {
    instance<NettyApplicationEngine>("start")
}