import io.bricks.api.UseCase
import io.bricks.sandbox.api.echo.Message
import io.bricks.sandbox.api.echo.SendEcho

class SendEchoInteractor : UseCase<SendEcho, Message> {
    override fun SendEcho.execute(): Message = message
}