package io.bricks.sandbox.api.echo

import io.bricks.api.Action
import io.bricks.network.core.ApiMethod

data class SendEcho(
    val message: Message
) :
    Action<Message>,
    ApiMethod<Message> {

    override val resultType = Message::class.java
}