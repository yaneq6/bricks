package io.bricks.sandbox.api.echo

data class Message(
    val text: String
)